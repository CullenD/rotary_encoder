/*
 * uart.h
 *
 *  Created on: Feb 4, 2016
 *      Author: robertvanvossen
 */

#ifndef UART_H_
#define UART_H_

//#include "common.h"

//void UartInit(void);
//void UartTx(char * tx_data);           // serial output transmitter
void putc ( void* p, char c);
//void UartHexAddrTx(uint16_t * addr);

void TimerA_UART_init(void);
void TimerA_UART_tx(unsigned char byte);
void TimerA_UART_print(char *string);
//void UartUlTx(int32_t val);
//void TxStateRecord(stateRecord_t *);
//void TxSystemRecord(systemRecord_t *);

#endif /* UART_H_ */
