//***************************************************************************************
//  MSP430
//
//  Description: SAF-HOLLAND single track decoder program. The program prints interpreted
//               values from the 8 ADC channels as an 8-bit number on P2.0 as 96008N1
//               serial data. The ADCs are intended to be hooked up to a A1324 hall
//               effect sensor (powered from 5V, with a 5.1k/10k resistor divider on the
//               output). It also prints the interpreted position based off of these values.
//               The software is capable of determining position from any 7 of the 8 read
//               bits (this allows a Vernier relationship between the target spacing and
//               the sensor spacing to ensure that only one sensor is indeterminate at a
//               time).
//
//  Alexander Hild /  Brian Collins
//
//***************************************************************************************

#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>
#include "printf.h"
#include "uart.h"
#include "sysClock.h"

/************************************************************
 * Application Parameters
 ***********************************************************/
#define WINDOW_SIZE 7
#define NUM_SENSORS (WINDOW_SIZE+1)

#define ADC_LOW_THRESHOLD   460
#define ADC_HIGH_THRESHOLD  490

#define PRINT_TIME_MS 1500

#define SINGLE_TRACK_LEN 126
int singleTrack[SINGLE_TRACK_LEN] = {0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1};


#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    __disable_interrupt();
    P1IFG &= ~BIT0;         // clear interrupt flag
    __enable_interrupt();
}

/************************************************************
 * Function Prototypes
 ***********************************************************/
// Hardware support
static void init(void);
void initADC(void);

// Position resolution
int16_t findPositionInSingleTrack(int16_t GraySegment[]);
int16_t resolvePosition(int16_t *window);
void selectNextChannel(uint16_t channel);
int16_t getBitValue(uint16_t ADCVal);


static void init(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer

	// Use calibration values for 1MHz Clock DCO
	DCOCTL  = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;


    // Set up ADC for power sensing, which will enable determining if we are in programming mode or not.
    // If we are, then power sense will read a very small voltage (<1.0V). Power monitoring code borrowed
    // and modified from StoPro V0.2.
    initADC();

    // UART pin
    TimerA_UART_init();

    // Set up printf support
    init_printf(NULL,putc);

    // Start the system clock
    initClock();
}

void initADC(void)
{
    // Configure power sense as an analog input
    ADC10AE0 = BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5 | BIT6 | BIT7;
    ADC10DTC1 = 0x0000;     // Disable automatic data transfers to memory, we don't need them.
    ADC10DTC0 = 0x0000;

    // ****** THIS REGISTER NEEDS TO BE SET BEFORE ADC10CTL0 - the ENC bit in that register prevents writing to parts of this register ******
    // INCH_0 - Select channel 0.
    // ADC10SC bit used as S&H source.
    // Straight binary format.
    // Clock divide by 8.
    // Use SMCLK - 1MHz/(8 * 64) = 1.95 kHz conversion rate.
    ADC10CTL1 = ADC10DIV0 | ADC10DIV1 | ADC10DIV2 | ADC10SSEL_3;


    // SREF0 - Vref positive reference & 0V negative reference,
    // ADC10SHT_3 - 64 clock cycles per sample & hold,
    // Interrupt is not enabled.
    // ENC - Conversion is enabled.
    ADC10CTL0 =  ADC10SHT_3 | ADC10ON | ENC;
}

int main(void)
{
    uint16_t    i               = 0;
    uint16_t    channel         = 0;
    int16_t     printTimer_ms   = PRINT_TIME_MS;
    uint16_t    lastSysTick_ms  = 0;
    int16_t     position        = 0;
    int16_t     window[NUM_SENSORS];
    uint16_t    sensorVals[NUM_SENSORS];

    init();

    // Clear the screen
    printf("\033[2J");

    // Print the column headers
    printf("\rSAF-HOLLAND Single Track Decoder\n\r ");
    /*for(l=0; l < NUM_SENSORS; l++)
    {
        printf("AD%1u  ", l);
    }
    */
    printf("    Code  Position\n");

	while(1) // 1MHz
	{
	    // Start conversion
	    ADC10CTL0 |= ADC10SC;

	    // Wait for the ADC conversion to complete
	    while(ADC10CTL1 & ADC10BUSY);

	    // Add ADC conversion value from selected channel to decoder array
	    sensorVals[channel] = ADC10MEM;

	    // Translate ADC value to 1 or 0 and store
	    window[channel] = getBitValue(sensorVals[channel]);

	    // Figure out where we are on the single track
	    position = resolvePosition(window);

	    // Select the next channel to read
	    selectNextChannel(channel);
	    channel++;

	    if(channel == NUM_SENSORS)
	    {
	        // Print channel reads periodically
	        printTimer_ms -= ( getSysTickMs() - lastSysTick_ms );
	        lastSysTick_ms = getSysTickMs();
	        if(printTimer_ms <= 0)
	        {
	            printf("\r ");
                for(i=0;i<NUM_SENSORS;i++)
                {
                    if( (window[i] == 1) || (window[i] == 0))
                    {
                        printf("%1u", window[i]);
                    }
                    else
                    {
                        printf("X");
                    }
                }

                printf("        %2d", position);

                printTimer_ms = PRINT_TIME_MS;
	        }

	        channel = 0;
	    }

	    // Updates the clock if 1ms or more has passed.
		if( getHardMilliseconds() )
		{
			updateClock();
		}
   	}

}

int16_t resolvePosition(int16_t *window)
{
    int16_t retVal;
    uint16_t i = 0;
    int16_t windowCopy[NUM_SENSORS];

    memcpy(windowCopy, window, NUM_SENSORS*sizeof(int16_t));

    // First, try replacing X values with 0
    for(i=0; i<NUM_SENSORS; i++)
    {
        if(window[i] == -1)
        {
            // Replace the X, record what was replaced
            windowCopy[i] = 0;
        }
    }

    // See if we found a valid position, return it if we did.
    retVal = findPositionInSingleTrack(windowCopy);
    if(retVal != -1)
    {
        return retVal;
    }

    // If we are here, no valid position found. Change X values to 1 instead, then see if we can resolve the position
    for(i=0; i<NUM_SENSORS; i++)
    {
        if(window[i] == -1)
        {
            // Replace the X, record what was replaced
            windowCopy[i] = 1;
        }
    }
    return findPositionInSingleTrack(windowCopy);
}


int16_t findPositionInSingleTrack(int16_t *window)
{
    uint16_t match = 0;
    uint16_t i = 0;
    uint16_t j = 0;
    const uint16_t length = SINGLE_TRACK_LEN - WINDOW_SIZE;

    for(i=0;i<length;i++)
    {
        if(singleTrack[i] == window[0])
        {
            match = 1;

            for(j=1;j<NUM_SENSORS;j++)
            {
                if(singleTrack[i+j] == window[j])
                {
                    match++;
                }
                else
                {
                    match = 0;
                }
            }

            if(match == NUM_SENSORS)
            {
                return i;
            }
        }
    }
    return -1;
}

void selectNextChannel(uint16_t channel)
{
    ADC10CTL0 &= ~ENC;  //set ENC bit to 0 to allow channel change

    ADC10CTL1 &= 0x0FFF;    //set INCH channels to zero before selecting next channel

    switch (channel){
    case 0:
        ADC10CTL1 |= INCH0;                     //A1
        break;
    case 1:
        ADC10CTL1 |= INCH1;                     //A2
        break;
    case 2:
        ADC10CTL1 |= (INCH1 | INCH0);           //A3
        break;
    case 3:
        ADC10CTL1 |= INCH2;                     //A4
        break;
    case 4:
        ADC10CTL1 |= (INCH2 | INCH0);           //A5
        break;
    case 5:
        ADC10CTL1 |= (INCH2 | INCH1);           //A6
        break;
    case 6:
        ADC10CTL1 |= (INCH2 | INCH1 | INCH0);   //A7
        break;
    case 7:
        ADC10CTL1 &= 0x0FFF;                    //A0
        break;
    }

    ADC10CTL0 |= ENC;
}

int16_t getBitValue(uint16_t ADCVal)
{
    if(ADCVal > ADC_HIGH_THRESHOLD)
    {
        return 1;
    }
    else if(ADCVal < ADC_LOW_THRESHOLD)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}
