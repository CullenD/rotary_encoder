/*
 * pwrMon.c
 *
 *  Created on: Jan 29, 2018
 *      Author: CollinsBr
 */

#include <msp430g2755.h>
#include <stdint.h>
#include <string.h>
#include "pwrMon.h"

#define TRUE 1
#define FALSE 0

const float     minInpV_LH      = 7.1;
const float     minInpV_HL      = 6.9;
const float     pwrOKMinInpV    = 8.0;
const float     pwrOKMaxInpV    = 32.0;

const float     minV_12             = 8.0;
const float     crankingMinV_12     = 6.5;
const float     crankingMaxV_12     = 9.0;
const float     engineOffMinV_12    = 9.0;
const float     engineOffMaxV_12    = 13.2;
const float     engineOnMinV_12     = 13.2;
const float     engineOnMaxV_12     = 16.0;
const float     maxV_12             = 16.0;

const float     thresholdV_12_24    = 17.0;

const float     minV_24             = 18.0;
const float     crankingMinV_24     = 13.3;
const float     crankingMaxV_24     = 18.0;
const float     engineOffMinV_24    = 18.0;
const float     engineOffMaxV_24    = 26.4;
const float     engineOnMinV_24     = 26.4;
const float     engineOnMaxV_24     = 32.0;
const float     maxV_24             = 32.0;

const float     resDivGain          = 13.4;
const float     refVoltage          = 2.5;
const uint16_t  ADCSteps            = 1023;
float           inpV                = 0.0;
uint16_t        pwrOK               = FALSE;
uint16_t        ignStep             = FALSE;

// We will implement a state machine to monitor the system voltage to track what the system is doing (cranking, engine running, etc).
typedef enum states
{
    DETERMINE_SYSTEM_VOLTAGE_STATE,
    ENGINE_ON_12V_STATE,
    ENGINE_OFF_12V_STATE,
    CRANKING_12V_STATE,
    ENGINE_ON_24V_STATE,
    ENGINE_OFF_24V_STATE,
    CRANKING_24V_STATE,
} states_t;

states_t state = DETERMINE_SYSTEM_VOLTAGE_STATE;

#define PWR_OK_DEBOUNCE_SAMPLES 10

// Function prototypes
void updateFSM(void);
void handleDetermineSystemVoltageState(void);
void handleEngineOn12VState(void);
void handleEngineOff12VState(void);
void handleCranking12VState(void);
void handleEngineOn24VState(void);
void handleEngineOff24VState(void);
void handleCranking24VState(void);

void checkInpVoltage(void)
{
    // Monitor input power. Power sufficient means there is power available that can run the device,
    // power OK means the power is within a normal range.
    static uint16_t pwrSufficient   = FALSE;
    static uint16_t pwrOKSample     = FALSE;
    static uint16_t pwrOKDebounce   = 0;

    // There is a voltage divider of 124k and 10k on input power. We need to multiply the ADC value by this gain (13.4) to turn a 0V-2.5V
    // signal into a 0V - 33.5V signal. The ADC rails above 33.5V (overvoltage shutoff happens between 33.4V and 39.8V - nominal 36.9V)
    const float voltsPerLSB = (resDivGain*refVoltage)/ADCSteps;


    // Wait for conversion to finish
    while(ADC10CTL1 & ADC10BUSY);

    inpV = ADC10MEM * voltsPerLSB;    // 10 bit ADC value. 1023 steps ranging from 0V to 2.5V -> floating point value from 0V to 33.5V.

    // Undervoltage protect.
    if((pwrSufficient == TRUE) && (inpV < minInpV_HL))
    {
        pwrSufficient = FALSE;
    }
    else if((pwrSufficient == FALSE) && (inpV > minInpV_LH))
    {
        pwrSufficient = TRUE;
    }
    else if(pwrSufficient == FALSE)
    {
        pwrSufficient = FALSE;
    }

    // Determine if power is within a valid range, set pwrOKSample accordingly
    if( (inpV > pwrOKMinInpV) && (inpV < pwrOKMaxInpV) )
    {
        pwrOKSample = TRUE;
    }
    else
    {
        pwrOKSample = FALSE;
    }

    // Debounce pwrOKSample to generate pwrOK.
    if (pwrOKSample == pwrOK) // no pending change
    {
        pwrOKDebounce = 0;
    }
    else // pending change
    {
        pwrOKDebounce++;
        if (pwrOKDebounce  >= PWR_OK_DEBOUNCE_SAMPLES) // change state
        {
            pwrOK = !pwrOK;
            pwrOKDebounce = 0;
        }
    }

    updateFSM();

    ADC10CTL0 |= ADC10SC; // Start next conversion
}

void initADC(void)
{
    // Configure power sense as an analog input
    ADC10AE0 = BIT0;        // Configure power sense pin (P2.0 - A0) as analog input
    ADC10DTC1 = 0x0000;     // Disable automatic data transfers to memory, we don't need them.
    ADC10DTC0 = 0x0000;

    // ****** THIS REGISTER NEEDS TO BE SET BEFORE ADC10CTL1 - the ENC bit in that register prevents writing to parts of this register ******
    // INCH_0 - Select channel 0.
    // ADC10SC bit used as S&H source.
    // Straight binary format.
    // Clock divide by 8.
    // Use SMCLK - 1MHz/(8 * 64) = 1.95 kHz conversion rate.
    // Repeatedly convert single channel.
    ADC10CTL1 = INCH_0 | ADC10DIV0 | ADC10DIV1 | ADC10DIV2 | ADC10SSEL_3;// | CONSEQ1;

    // MSC - Multiple sample and convert (continuously do conversions),
    // SREF0 - Vref positive reference & 0V negative reference,
    // ADC10SHT_3 - 64 clock cycles per sample & hold,
    // Interrupt is not enabled.
    // ENC - Conversion is enabled.
    ADC10CTL0 = MSC | SREF0 | ADC10SHT_3 | REF2_5V | REFON | ADC10ON | ENC;

    ADC10CTL0 |= ADC10SC; // Start conversion
}

void updateFSM(void)
{
    switch(state)
    {
    case DETERMINE_SYSTEM_VOLTAGE_STATE:
        handleDetermineSystemVoltageState();
        break;
    case ENGINE_ON_12V_STATE:
        handleEngineOn12VState();
        break;
    case ENGINE_OFF_12V_STATE:
        handleEngineOff12VState();
        break;
    case CRANKING_12V_STATE:
        handleCranking12VState();
        break;
    case ENGINE_ON_24V_STATE:
        handleEngineOn24VState();
        break;
    case ENGINE_OFF_24V_STATE:
        handleEngineOff24VState();
        break;
    case CRANKING_24V_STATE:
        handleCranking24VState();
        break;
    }

}

void handleDetermineSystemVoltageState(void)
{
    if( (inpV > minV_12) && (inpV < maxV_12) )
    {
        state   = ENGINE_ON_12V_STATE;
        ignStep = FALSE;
    }
    else if( (inpV > minV_24) && (inpV < maxV_24) )
    {
        state   = ENGINE_ON_24V_STATE;
        ignStep = FALSE;
    }
    else
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
}

void handleEngineOn12VState(void)
{
    if( inpV > engineOnMaxV_12 ) // We see more than a 12V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_12) // Engine is cranking (trying to start)
    {
        state   = CRANKING_12V_STATE;
        ignStep = FALSE;
    }
    else if( (inpV > engineOffMinV_12) && (inpV < engineOffMaxV_12) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_12V_STATE;
        ignStep = FALSE;
    }
    else if( (inpV > engineOnMinV_12) && (inpV < engineOnMaxV_12) ) // Engine is running
    {
        state   = ENGINE_ON_12V_STATE;
    }
}

void handleEngineOff12VState(void)
{
    if( inpV > engineOnMaxV_12 ) // We see more than a 12V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_12) // Engine is cranking (trying to start)
    {
        state   = CRANKING_12V_STATE;
    }
    else if( (inpV > engineOffMinV_12) && (inpV < engineOffMaxV_12) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_12V_STATE;
    }
    else if( (inpV > engineOnMinV_12) && (inpV < engineOnMaxV_12) ) // Engine is running
    {
        state   = ENGINE_ON_12V_STATE;
        ignStep = TRUE;
    }
}

void handleCranking12VState(void)
{
    if( inpV > engineOnMaxV_12 ) // We see more than a 12V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_12) // Engine is cranking (trying to start)
    {
        state   = CRANKING_12V_STATE;
    }
    else if( (inpV > engineOffMinV_12) && (inpV < engineOffMaxV_12) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_12V_STATE;
    }
    else if( (inpV > engineOnMinV_12) && (inpV < engineOnMaxV_12) ) // Engine is running
    {
        state   = ENGINE_ON_12V_STATE;
        ignStep = TRUE;
    }
}

void handleEngineOn24VState(void)
{
    if( inpV < crankingMinV_24 ) // We see less than a 24V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_24) // Engine is cranking (trying to start)
    {
        state   = CRANKING_24V_STATE;
        ignStep = FALSE;
    }
    else if( (inpV > engineOffMinV_24) && (inpV < engineOffMaxV_24) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_24V_STATE;
        ignStep = FALSE;
    }
    else if( (inpV > engineOnMinV_24) && (inpV < engineOnMaxV_24) ) // Engine is running
    {
        state   = ENGINE_ON_24V_STATE;
    }
}
void handleEngineOff24VState(void)
{
    if( inpV < crankingMinV_24 ) // We see less than a 24V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_24) // Engine is cranking (trying to start)
    {
        state   = CRANKING_24V_STATE;
    }
    else if( (inpV > engineOffMinV_24) && (inpV < engineOffMaxV_24) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_24V_STATE;
    }
    else if( (inpV > engineOnMinV_24) && (inpV < engineOnMaxV_24) ) // Engine is running
    {
        state   = ENGINE_ON_24V_STATE;
        ignStep = TRUE;
    }
}
void handleCranking24VState(void)
{
    if( inpV < crankingMinV_24 ) // We see less than a 24V system should exhibit - need to re-evaluate how we are being powered
    {
        state = DETERMINE_SYSTEM_VOLTAGE_STATE;
    }
    else if( inpV < crankingMaxV_24) // Engine is cranking (trying to start)
    {
        state   = CRANKING_24V_STATE;
    }
    else if( (inpV > engineOffMinV_24) && (inpV < engineOffMaxV_24) ) // System is running off of battery power
    {
        state   = ENGINE_OFF_24V_STATE;
    }
    else if( (inpV > engineOnMinV_24) && (inpV < engineOnMaxV_24) ) // Engine is running
    {
        state   = ENGINE_ON_24V_STATE;
        ignStep = TRUE;
    }
}

uint16_t ignStepDetected(void)
{
    return ignStep;
}

void clearIgnStep(void)
{
    ignStep = FALSE;
}
