//***************************************************************************************
//  MSP430
//
//  Description: SAF-HOLLAND Coupling Monitor (ELI Gen II) PCB EE-11972-T1
//               Functional program with serial output and flash storage.
//
//  Andrew Wallner /  Brian Collins
//
//***************************************************************************************

//#undef LAMPS
#define LAMPS

#include <msp430g2755.h>
#include <stdint.h>
#include <string.h>
#include "printf.h"
#include "uart.h"
#include "common.h"
#include "flash.h"
#include "pwrMon.h"
#include "hal.h"
#include "sysClock.h"
#include "boardBringUp.h"

/************************************************************
 * Application Parameters
 ***********************************************************/
#define INIT_TIME_MS                                            2000    // time lamps are asserted at power-up (after report from flash)
#define REINIT_TIME_MS                                          12
#define OUTPUT_INIT_TIME_MS                                     10      // time outputs are asserted at power-up (after report from flash)
#define LOAD_SWITCH_OVER_CURRENT_TEST_TIME_MS                   0       // time after which load switch is tested for over current (must be 0 since OC protection signal asserts in 50 us)
#define LOAD_SWITCH_OVER_CURRENT_TEST_TIME_NOPS                 50      // no-ops after which load switch is tested for over current (OC protection signal asserts in 50 us)
#define LOAD_SWITCH_OPEN_CIRCUIT_TEST_TIME_MS                   5       // time after which load switch is tested for open circuitcurrent (open circuit signals asserts in 700 us)
#define MAX_INSPECTION_LAMP_TIME_MINUTES                        5       // inspection lamp time-out
#define MAX_LKS_DELAY_TIME_MS                                   1000    // slow-lock time limit
#define MAX_LKS_EARLY_TIME_MS                                   1000    // slow kp flag time limit ***W-M rev1
#define BLINK_HALF_PERIOD_MS                                    100     // sets warning lamp flash rate
#define MAX_TIME_AFTER_UNLOCK_BEFORE_ALARM_MS                   3000    // time before warning output is asserted while in unlock state. 65000 milli-seconds max
#define MAX_ERROR_TIME_MINUTES                                  5       // time in error state before state machine is automatically re-initialized ***W-M rev1
#define NOPS_AT_POWER_DOWN                                      100000  // wait about 1 second if power loss is detected before attempting to reinitialize
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS       200     // glitch avoidance (debounce) for kp sensor before coupling
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS     1000    // glitch avoidance (during travel over curbs and railroad tracks, etc.) for kp sensor
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_INSPECT_MS    200     // (MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS / 8)


#define kps_OVERFLOW_FLAG (1 << 0)
#define KPSR_OVERFLOW_FLAG (1 << 1)
#define LKS_OVERFLOW_FLAG (1 << 2)


#define MAX_INT_VAL 0xFFFFFFFF //check where this is used. Many ints are 16b !

/************************************************************
 * Custom Data Types
 ***********************************************************/
// States
// Note: other structures are dependent on the order and quantity of this enum.
typedef enum
{
	UNINIT_STATE,           //notes:
	INIT_STATE,				//print from flash
	WAIT_PWR_OK_STATE,      //write to flash upon state entry
	OPEN_STATE,				//write to flash
	INDETERMINATE_STATE,
	INSPECTION_STATE,
	LOCK_MONITORING_STATE,	//write to flash
	UNLOCKED_STATE,
	ERROR_STATE				//write to flash
}
states_t;

typedef enum
{
    NO_COLOR,
    RED,
    GREEN,
    BLUE,
    YELLOW,
    PURPLE,
    CYAN,
    WHITE
}
colors_t;

typedef enum
{
    WARNING_SIGNAL,
    COUPLE_SIGNAL
}
signals_t;

/************************************************************
 * Function Prototypes
 ***********************************************************/
// Hardware support
static void activateWarningOutputs(void);
static void ledControl(colors_t color);
static void outputSignalControl(signals_t sig, uint8_t active);
static void enterLowPowerMode(void);
static void init(void);

#ifdef RECORDS
static void updateStateRecord(stateRecord_t *);
static void updateRecordsAndLogState(systemRecord_t * sr, states_t lastState);
#endif

// FSM functions
static void handleUninitState(uint8_t stateChanged);
static void handleInitState(uint8_t stateChanged);
static void handleWaitPwrOKState(uint8_t stateChanged);
static void handleOpenState(uint8_t stateChanged);
static void handleIndeterminateState(uint8_t stateChanged);
static void handleInspectionState(uint8_t stateChanged);
static void handleLockMonitoringState(uint8_t stateChanged);
static void handleUnlockedState(uint8_t stateChanged);
static void handleErrorState(uint8_t stateChanged);
static void updateState(void);


/************************************************************
 * Variables (Global and Local)
 ***********************************************************/
// Application support
uint16_t initTimer = INIT_TIME_MS;
uint16_t initTimerMaxValue = INIT_TIME_MS;
uint16_t coupleCount = 0;
uint16_t errorCount = 0;
uint32_t timeCoupled_min = 0;
uint16_t unlockedTime_ms = 0;
uint32_t timeError_min = 0;

states_t state = UNINIT_STATE;
states_t lastState = UNINIT_STATE;

uint16_t loadSwitchState =  0xFF00; //ST_WL | ST_IL;

#ifdef RECORDS

systemRecord_t recordCurrentSystem =
{
	.tag = SYST_TAG,
	.sysTime_ms = 0,                      
	.sysTime_min = 0,                     
    
	.countINIT_STATE = 0,			      
	.countOPEN_STATE = 0,			      
	.countINDETERMINATE_STATE = 0,        
	.countINSPECTION_STATE = 0,           
	.countLOCK_MONITORING_STATE = 0,      
	.countUNLOCKED_STATE = 0,            
	.countERROR_STATE = 0,	              
    
	.elapsedMinutesINIT_STATE = 0,		 
	.elapsedMinutesOPEN_STATE = 0,		 
	.elapsedMinutesINDETERMINATE_STATE = 0,  
	.elapsedMinutesINSPECTION_STATE = 0,     
	.elapsedMinutesLOCK_MONITORING_STATE = 0,
	.elapsedMinutesUNLOCKED_STATE = 0,       
	.elapsedMinutesERROR_STATE = 0,	        
    
	.totalErrOverflowFlags = 0,              
	.totalKpsErr_ms = 0,
	.totalloadSwitchState = 0,
	.totalLksErr_ms = 0,                     
};

stateRecord_t recordINIT_STATE;
stateRecord_t recordOPEN_STATE;
stateRecord_t recordINDETERMINATE_STATE;
stateRecord_t recordINSPECTION_STATE;
stateRecord_t recordLOCK_MONITORING_STATE;
stateRecord_t recordUNLOCKED_STATE;
stateRecord_t recordERROR_STATE;

static void updateStateRecord(stateRecord_t * sr)
{
	// run this function on lastState at state change
	sr->sysTime_ms  = getSysTickMs();      // timestamp at state exit, when err values are known
	sr->sysTime_min = getSysTickMin();     // timestamp at state exit, when err values are known
	sr->kpsErr_ms  = kpsErr_ms;
	sr->lksErr_ms   = lksErr_ms;
	sr->loadSwitchState = loadSwitchState;
}

static void updateRecordsAndLogState(systemRecord_t * sr, states_t lastState)
{
	// this function is to be run at state change
	
	uint16_t elapsedMinutes = 0; // For now, milliseconds are not counted.

    // compute elapsed time since last state change.
	
	if (getSysTickMin() > sr->sysTime_min)
	{
		elapsedMinutes = (getSysTickMin() - sr->sysTime_min);
	}
	else
	{
		elapsedMinutes = 0;
	}
 	
	sr->sysTime_min = getSysTickMin();     // timestamp at state exit
	sr->sysTime_ms  = getSysTickMs();      // timestamp at state exit
    
	
	switch(lastState)
	{
		case INIT_STATE:
			sr->countINIT_STATE++;			          
			sr->elapsedMinutesINIT_STATE += elapsedMinutes;
			updateStateRecord(&recordINIT_STATE);
			Log(&recordINIT_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINIT_STATE);
#endif
			break;
		case OPEN_STATE:
			sr->countOPEN_STATE++;
			sr->elapsedMinutesOPEN_STATE += elapsedMinutes;
			updateStateRecord(&recordOPEN_STATE);
			Log(&recordOPEN_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordOPEN_STATE);
#endif
			break;
		case INDETERMINATE_STATE:
			sr->countINDETERMINATE_STATE++;            
			sr->elapsedMinutesINDETERMINATE_STATE += elapsedMinutes;
			updateStateRecord(&recordINDETERMINATE_STATE);
			Log(&recordINDETERMINATE_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINDETERMINATE_STATE);
#endif
			break;
		case INSPECTION_STATE:
			sr->countINSPECTION_STATE++;
			sr->elapsedMinutesINSPECTION_STATE += elapsedMinutes;
			updateStateRecord(&recordINSPECTION_STATE);
			Log(&recordINSPECTION_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINSPECTION_STATE);
#endif
			break;
		case LOCK_MONITORING_STATE:	
			sr->countLOCK_MONITORING_STATE++;          
			sr->elapsedMinutesLOCK_MONITORING_STATE += elapsedMinutes;
			updateStateRecord(&recordLOCK_MONITORING_STATE);
			Log(&recordLOCK_MONITORING_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordLOCK_MONITORING_STATE);
#endif
			break;
		case UNLOCKED_STATE:
			sr->countUNLOCKED_STATE++;                 
			sr->elapsedMinutesUNLOCKED_STATE += elapsedMinutes;		
			updateStateRecord(&recordUNLOCKED_STATE);
			Log(&recordUNLOCKED_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordUNLOCKED_STATE);
#endif
			break;
		case ERROR_STATE:
			sr->countERROR_STATE++;	                
			sr->elapsedMinutesERROR_STATE += elapsedMinutes;			
			updateStateRecord(&recordERROR_STATE);
			Log(&recordERROR_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordERROR_STATE);
#endif
			break;		
		default:
			break;
	}

	
	if (kpsErr_ms > (MAX_INT_VAL - sr->totalKpsErr_ms)) sr->totalErrOverflowFlags |= kps_OVERFLOW_FLAG; //set overflow flag

	if (lksErr_ms > (MAX_INT_VAL - sr->totalLksErr_ms))  sr->totalErrOverflowFlags |= LKS_OVERFLOW_FLAG;
	
	sr->totalKpsErr_ms += (uint32_t) kpsErr_ms;       //  uint32_t

	sr->totalLksErr_ms  += (uint32_t) lksErr_ms;        //  uint32_t
	
	kpsErr_ms = 0;

	lksErr_ms = 0;		

}

#endif //RECORDS

static void init(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer

	// Use calibration values for 1MHz Clock DCO
	DCOCTL  = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;

	// Zero out records
#ifdef RECORDS
	recordINIT_STATE = 				(stateRecord_t){INIT_TAG, 0, 0 ,0, 0, 0};
	recordOPEN_STATE = 				(stateRecord_t){OPEN_TAG, 0, 0 ,0, 0, 0};
	recordINDETERMINATE_STATE = 	(stateRecord_t){INDETERMINATE_TAG, 0, 0 ,0, 0, 0};
	recordINSPECTION_STATE = 		(stateRecord_t){INSPECTION_TAG, 0, 0 ,0, 0, 0};
	recordLOCK_MONITORING_STATE = 	(stateRecord_t){LOCK_MONITORING_TAG, 0, 0 ,0, 0, 0};
	recordUNLOCKED_STATE = 			(stateRecord_t){UNLOCKED_TAG, 0, 0 ,0, 0, 0};
	recordERROR_STATE = 			(stateRecord_t){ERROR_TAG, 0, 0 ,0, 0, 0};
#endif

    // Set up ADC for power sensing, which will enable determining if we are in programming mode or not.
    // If we are, then power sense will read a very small voltage (<1.0V). Power monitoring code borrowed
    // and modified from StoPro V0.2.
    initADC();

    // Start the system clock
    initClock();
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
int main(void)
{
    //BMC: Tests used for board bring-up & debug.
    //runBoardBringUpTest(P18V_P12V_REG_POINT);
    //runBoardBringUpTest(P12V_LOW_POWER_MODE);
    //runBoardBringUpTest(SPREAD_SPECTRUM_DISABLE);
    //runBoardBringUpTest(CPL_TEST_HSS);
    //runBoardBringUpTest(CPL_TEST_LSS);
    //runBoardBringUpTest(WARN_TEST_HSS);
    //runBoardBringUpTest(WARN_TEST_LSS);
    //runBoardBringUpTest(SER_TX_CAN_CTL_WARN);
    //runBoardBringUpTest(CAN_MODE_DISCRETE_OUTPUT_OVERRIDE);
    //runBoardBringUpTest(INSPECTION_LIGHT);
    //runBoardBringUpTest(WARN_LIGHT);

    init();

    // Monitor for power going away! Power loss detection delay of 10ms (10ms debounce).
    uint16_t pwrOKLastState = pwrOK;

	while(1) //1MHz
	{
	    if(pwrOK == FALSE && pwrOKLastState == TRUE)
	    {
	        // Power loss! Enter low power mode, write the logs to flash, then enter uninit state which will wait for power return.
	        enterLowPowerMode();
	        updateRecordsAndLogState(&recordCurrentSystem, lastState);
	        state = UNINIT_STATE;
	    }

	    // Save value of pwrOK
	    pwrOKLastState = pwrOK;

	    // updates the state machine if 1ms has passed. Also updates the state of the inputs, including pwrOK.
		if( getHardMilliseconds() )
		{
			updateClock();
			readInputs();
			updateState();
		}
   	}

}


void updateState(void)
{
	uint8_t stateChanged = FALSE;
	if (state != lastState)
	{
		if ((lastState == ERROR_STATE) ) // warning signal could be on, so serial transmit won't work.
		{
		    warnSetState(INACTIVE);
		}

		updateRecordsAndLogState(&recordCurrentSystem, lastState);

		lastState = state;
		stateChanged = TRUE;
	}

	// Check to make sure power is still good.
	if((state != UNINIT_STATE) && (state != INIT_STATE) && (pwrOK == FALSE) )
	{
	    state = WAIT_PWR_OK_STATE;
	}

	// Check for an ignition step - if we saw one, need to reset the unit
	if( ignStepDetected() )
	{
	    state = UNINIT_STATE;
	    clearIgnStep();
	}

	switch(state)
	{
	case UNINIT_STATE:
		handleUninitState(stateChanged);
		break;
	case INIT_STATE: //reset timers and activate indicators, check inputs
		handleInitState(stateChanged);
		break;
	case WAIT_PWR_OK_STATE:
	    handleWaitPwrOKState(stateChanged);
	    break;
	case OPEN_STATE: //wait for inputs, (ready to couple)
		handleOpenState(stateChanged);
		break;
	case INDETERMINATE_STATE: //not ready to couple, check time of sensor assertion  (use kpTimeAsserted_ms)
		handleIndeterminateState(stateChanged);
		break;
	case INSPECTION_STATE: //indicate locked
		handleInspectionState(stateChanged);
		break;
	case LOCK_MONITORING_STATE: //indicate locked
		handleLockMonitoringState(stateChanged);
		break;
	case UNLOCKED_STATE: //loss of lock, indicate pending error
		handleUnlockedState(stateChanged);
		break;
	case ERROR_STATE: //indicate error
		handleErrorState(stateChanged);
		break;
	}
}

static void handleUninitState(uint8_t stateChanged)
{
    setSysTickMs(recordCurrentSystem.sysTime_ms);
    setSysTickMin(recordCurrentSystem.sysTime_min);

	// If input voltage reads less than 1V but we are powered on and running, then the device is being
	// programmed. If this is the case, don't initialize anything, just wait to be programmed.
	if(inpV > 1.0)
	{
	    state = INIT_STATE;
	    initHAL();
	    SelectEmptyLog();
	}
	else
	{
	    state = UNINIT_STATE;
	}
}

static void handleInitState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		//UartTx("INIT_STATE\r\n");
		printf("INIT_STATE\r\n");
#endif

		// Reset init timer
		initTimer = INIT_TIME_MS;

		// Delay to ensure print output before WARNING_LAMP_INIT
		uint16_t i;
		for(i=10000 ;i>0;i--)
		{
			_nop();
		}

		// Turn on outputs
#ifdef LAMPS
		inspLightSetState(ACTIVE);
		warnLightSetState(ACTIVE);
#endif
		cplSetState(ACTIVE);
		// Cannot do this right now, as the serial port may be active at this time
		// cplSetState(ACTIVE); //for test only? this could annoyingly activate an external horn

		unlockedTime_ms = 0;

		setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS);
	}

	if (initTimer == (initTimerMaxValue - OUTPUT_INIT_TIME_MS)) //turn off output signals (to prevent nuisance horn activation or prolonged dash gauge signal ambiguity)
	{
	    warnSetState(INACTIVE);
	    cplSetState(INACTIVE);
	}

	if (initTimer > 0) //value set at power-on-reset, and below for re-initialize
	{
		initTimer--;
	}
	else //initialization time is passed
	{
		initTimer = REINIT_TIME_MS;
		initTimerMaxValue = REINIT_TIME_MS;
		// Turn off all outputs

#ifdef LAMPS
        inspLightSetState(INACTIVE);
        warnLightSetState(INACTIVE);
#endif
		outputSignalControl(WARNING_SIGNAL, FALSE);
		outputSignalControl(COUPLE_SIGNAL, FALSE);

#ifdef PRINT_SWITCH_STATUS_AFTER_INIT
		printf(" inputs test: KP=");
		if (KPS_IS_ACTIVE) printf("on (grounded)");
		else printf("off (pulled-up)");

		printf(" LK=");
		if (LKS_IS_ACTIVE) printf("on (grounded)");
		else printf("off (pulled-up)");

		printf("\r\n");

#endif

		// Serial transmission is complete. Enable discrete functionality of the red control and the warning output.
		uartDisable();
		warnOutputEnable();
		redLEDEnable();

		if(pwrOK)
		{
            if((kp == TRUE) && (lks == TRUE))
            {
                state = INSPECTION_STATE; //allow jump to INSPECTION_STATE only at power-on
            }

            if ((kp == FALSE) && (lks == FALSE))
            {
                state = OPEN_STATE;
            }
            else
            {
                state = INDETERMINATE_STATE;
            }
		}
		else
		{
		    state = WAIT_PWR_OK_STATE;
		}
	}
}

static void handleWaitPwrOKState(uint8_t stateChanged)
{

#ifdef LAMPS
    inspLightSetState(INACTIVE);
    warnLightSetState(INACTIVE);
#endif

    outputSignalControl(WARNING_SIGNAL, FALSE);
    outputSignalControl(COUPLE_SIGNAL, FALSE);

    if(stateChanged && (pwrOK == FALSE))
    {
        // Power went out of range, better save everything off to flash because it is likely to go away totally.
        updateRecordsAndLogState(&recordCurrentSystem, lastState);
        state = WAIT_PWR_OK_STATE;
    }
    else if (pwrOK == TRUE)
    {
        if ((kp == FALSE) && (lks == FALSE))
        {
            state = OPEN_STATE;
        }
        else
        {
            state = INDETERMINATE_STATE;
        }
    }
    else
    {
        // Still waiting on power to enter the normal range.
        state = WAIT_PWR_OK_STATE;
    }
}

static void handleOpenState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("OPEN_STATE\r\n");
#endif
		// Turn off all outputs

#ifdef LAMPS
	    inspLightSetState(INACTIVE);
	    warnLightSetState(INACTIVE);
#endif
        outputSignalControl(WARNING_SIGNAL, FALSE);
        outputSignalControl(COUPLE_SIGNAL, FALSE);

		unlockedTime_ms = 0;

		setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS);

#ifdef TX_SYSTEM_RECORD_IN_OPEN_STATE
		TxSystemRecord(&recordCurrentSystem);
#endif
	}

	if ((kp == TRUE) || (lks == TRUE))
	{
		state = INDETERMINATE_STATE;
	}
}

static void handleIndeterminateState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("INDETERMINATE_STATE\r\n");
#endif
		// Turn off all outputs

#ifdef LAMPS
	    inspLightSetState(INACTIVE);
	    warnLightSetState(INACTIVE);
#endif
        outputSignalControl(WARNING_SIGNAL, FALSE);
        outputSignalControl(COUPLE_SIGNAL, FALSE);

        setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS);
	}

	if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE)) //normal sequence. wait for lks, else timeout error: lks-delay
	{
		if ((getSysTickMs() - kpTimeAsserted_ms) >= MAX_LKS_DELAY_TIME_MS)
		{
			state = ERROR_STATE;  //TODO ALW NOTE: verify rollover performance, assumes kpTimeAsserted_ms is valid
		}
		else
		{
			state = INDETERMINATE_STATE;
		}
	}
	else if ((kp == FALSE) && (lks == TRUE)) //abnormal sequence. error: no kingpin
	{
		if ((getSysTickMs() - lksTimeAsserted_ms) >= MAX_LKS_EARLY_TIME_MS) //reject abnormal sequence if lock was not recently detected e.g. slow flag assembly
		{
			state = ERROR_STATE;  //TODO ALW NOTE: verify rollover performance, assumes kpTimeAsserted_ms is valid
		}
		else
		{
			state = INDETERMINATE_STATE;
		}

		//state = ERROR_STATE;
	}
	else //(kp == TRUE) && (lks == TRUE)
	{
		state = INSPECTION_STATE; //normal sequence. lks detected after kingpin
	}

}

static void handleInspectionState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("INSPECTION_STATE\r\n");
#endif
		coupleCount++;
		timeCoupled_min = getSysTickMin();

		// Turn on Inspection light and coupling output
#ifdef LAMPS
        inspLightSetState(ACTIVE);
        warnLightSetState(INACTIVE);
#endif
        outputSignalControl(WARNING_SIGNAL, FALSE);
        outputSignalControl(COUPLE_SIGNAL, TRUE);

		// slow reaction time to accommodate kingpin movement, but not as slow as while locked.
        setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_INSPECT_MS);
	}

	if ((getSysTickMin() - timeCoupled_min) >= MAX_INSPECTION_LAMP_TIME_MINUTES)
	{
		state = LOCK_MONITORING_STATE;
	}
	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //loss of lock, pending error: no lock
	}
	else if ((kp == FALSE) && (lks == TRUE))
	{
		state = ERROR_STATE; //loss of kingpin
	}
	else //((kp == TRUE) && (lks == TRUE))
	{
		state = INSPECTION_STATE;
	}
}

static void handleLockMonitoringState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("LOCK_MONITORING_STATE\r\n");
#endif
		// Turn on Coupling output
#ifdef LAMPS
        inspLightSetState(INACTIVE);
        warnLightSetState(INACTIVE);
#endif
        outputSignalControl(WARNING_SIGNAL, FALSE);
        outputSignalControl(COUPLE_SIGNAL, TRUE);

        // Slow reaction time to accommodate kingpin movement.
        setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS);
	}

	if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //loss of lock, pending error: no lock
	}
	else if ((kp == FALSE) && (lks == TRUE))
	{
		state = ERROR_STATE; //sustained loss of kingpin
	}
	else //((kp == TRUE) && (lks == TRUE))
	{
		state = LOCK_MONITORING_STATE;
	}
}

static void handleUnlockedState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("UNLOCKED_STATE\r\n");
#endif

#ifdef LAMPS
        inspLightSetState(INACTIVE);
#endif
        outputSignalControl(COUPLE_SIGNAL, FALSE);
        outputSignalControl(WARNING_SIGNAL, FALSE);

        setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS);
	}

	unlockedTime_ms++;
	if ((unlockedTime_ms >= MAX_TIME_AFTER_UNLOCK_BEFORE_ALARM_MS)
		|| ((kp == FALSE) && (lks == TRUE)) 	//invalid lock, error: no kingpin
		|| ((kp == TRUE) && (lks == TRUE))) 	//unverified lock; user must uncouple and open lock or cycle power to reset
	{
		state = ERROR_STATE;
	}
	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //persisting loss of lock, pending error: no lks
	}
}

static void handleErrorState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("ERROR_STATE\r\n");
#endif
		timeError_min = getSysTickMin();
		errorCount++;
#ifdef LAMPS
        inspLightSetState(INACTIVE);
#endif
		outputSignalControl(COUPLE_SIGNAL, FALSE);

		setKPDebounceTime(MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS);
	}

	activateWarningOutputs();

	if ((getSysTickMin() - timeError_min) >= MAX_ERROR_TIME_MINUTES)
	{
		state = INIT_STATE;
	}

	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else
	{
		//((kp == TRUE)&&(lks == FALSE)) persisting loss of lock, error: no lks
		//((kp == FALSE)&&(lks == TRUE)) persisting loss of lock, error: no kingpin
		//((kp == TRUE)&&(lks == TRUE)) unverified lock; user must uncouple and open lock or cycle power to reset
		state = ERROR_STATE;
	}

}

static void activateWarningOutputs(void)
{
#ifdef LAMPS
    static uint16_t warningLampState = INACTIVE;
    static uint16_t blinkTimer = BLINK_HALF_PERIOD_MS;

	blinkTimer--;

	if (blinkTimer <= 0)
	{
		blinkTimer = BLINK_HALF_PERIOD_MS;

		if(warningLampState == INACTIVE)
		{
		    warningLampState = ACTIVE;
		}
		else
		{
		    warningLampState = INACTIVE;
		}
		warnLightSetState(warningLampState);
	}
#endif

    if (state != UNLOCKED_STATE)
    {
        warnSetState(ACTIVE);
    }
}


static void ledControl(colors_t color)
{
    switch(color){
    case NO_COLOR:
        redLEDSetState(INACTIVE);
        greenLEDSetState(INACTIVE);
        blueLEDSetState(INACTIVE);
        break;
    case RED:
        redLEDSetState(ACTIVE);
        greenLEDSetState(INACTIVE);
        blueLEDSetState(INACTIVE);
        break;
    case GREEN:
        redLEDSetState(INACTIVE);
        greenLEDSetState(ACTIVE);
        blueLEDSetState(INACTIVE);
        break;
    case BLUE:
        redLEDSetState(INACTIVE);
        greenLEDSetState(INACTIVE);
        blueLEDSetState(ACTIVE);
        break;
    case YELLOW:
        redLEDSetState(ACTIVE);
        greenLEDSetState(ACTIVE);
        blueLEDSetState(INACTIVE);
        break;
    case PURPLE:
        redLEDSetState(ACTIVE);
        greenLEDSetState(INACTIVE);
        blueLEDSetState(ACTIVE);
        break;
    case CYAN:
        redLEDSetState(INACTIVE);
        greenLEDSetState(ACTIVE);
        blueLEDSetState(ACTIVE);
        break;
    case WHITE:
        redLEDSetState(ACTIVE);
        greenLEDSetState(ACTIVE);
        blueLEDSetState(ACTIVE);
        break;
    }
}

static void outputSignalControl(signals_t sig, uint8_t active)
{
    if(sig == WARNING_SIGNAL)
    {
        if(active == FALSE)
        {
            warnSetState(INACTIVE);
            ledControl(NO_COLOR);
        }
        else
        {
            warnSetState(ACTIVE);
            ledControl(RED);
        }
    }
    else if(sig == COUPLE_SIGNAL)
    {
        if(active == FALSE)
        {
            cplSetState(INACTIVE);
            ledControl(NO_COLOR);
        }
        else
        {
            cplSetState(ACTIVE);
            ledControl(GREEN);
        }
    }
}

void enterLowPowerMode(void)
{
    // Regulator in low power mode
    p12VLowPowerMode();
    spreadSpectrumDisable();

    // Disable all outputs
    inspLightDisable();
    warnLightDisable();

    warnOutputDisable();
    cplOutputDisable();

    canModeDisable();
    uartDisable();

    redLEDDisable();
    greenLEDDisable();
    blueLEDDisable();

}
