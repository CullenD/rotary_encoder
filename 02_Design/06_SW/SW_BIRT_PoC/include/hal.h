/*
 * hal.h
 *
 *  Created on: Apr 12, 2018
 *      Author: CollinsBr
 */

#include "common.h"

#ifndef INCLUDE_HAL_H_
#define INCLUDE_HAL_H_


/************************************************************
 * Output States
 ***********************************************************/
enum
{
    INACTIVE,
    ACTIVE,
};

/************************************************************
 * Function Prototypes
 ***********************************************************/
void p12VHighPowerMode(void);
void p12VLowPowerMode(void);

void cplSelectHSS(void);
void cplSelectLSS(void);
void cplSetState(uint16_t state);
void cplOutputDisable(void);
void cplOutputEnable(void);

void warnSelectHSS(void);
void warnSelectLSS(void);
void warnSetState(uint16_t state);
void warnOutputDisable(void);
void warnOutputEnable(void);

void redLEDEnable(void);
void redLEDDisable(void);
void redLEDSetState(uint16_t state);

void greenLEDEnable(void);
void greenLEDDisable(void);
void greenLEDSetState(uint16_t state);

void blueLEDEnable(void);
void blueLEDDisable(void);
void blueLEDSetState(uint16_t state);

void canModeEnable(void);
void canModeDisable(void);

void spreadSpectrumEnable(void);
void spreadSpectrumDisable(void);

void inspLightEnable(void);
void inspLightDisable(void);
void inspLightSetState(uint16_t state);

void warnLightEnable(void);
void warnLightDisable(void);
void warnLightSetState(uint16_t state);

void uartEnable(void);
void uartDisable(void);

void setKPDebounceTime(uint16_t debounceTime);
void readInputs(void);
void initHAL(void);

/************************************************************
 * Running record of sensor state information
 ***********************************************************/
extern uint16_t kp;                     // combined kingpin state variable (legacy of single-kingpin sensor system)
extern uint16_t kps;                    // kingpin sensor
extern uint16_t lks;                    // lock sensor
extern uint16_t kpTimeAsserted_ms;      // timestamp of assertion; elapsed time must be calculated
extern uint16_t lksTimeAsserted_ms;

#ifdef COUNT_SENSOR_ERROR
extern uint16_t kpsErr_ms;
extern uint16_t lksErr_ms;
#endif

/************************************************************
 * Useful Macros
 ***********************************************************/
// Sensors have an active-low signal so we invert the reading
#define LKS_IS_ACTIVE (!(P4IN & BIT6))
#define KPS_IS_ACTIVE (!(P4IN & BIT5))

#endif /* INCLUDE_HAL_H_ */
