/*
 * boardBringUp.h
 *
 *  Created on: Mar 22, 2018
 *      Author: CollinsBr
 */

#ifndef INCLUDE_BOARDBRINGUP_H_
#define INCLUDE_BOARDBRINGUP_H_

// List of tests that can be executed
enum
{
    P18V_P12V_REG_POINT,
    P12V_LOW_POWER_MODE,
    SPREAD_SPECTRUM_DISABLE,
    CPL_TEST_HSS,
    WARN_TEST_HSS,
    CPL_TEST_LSS,
    WARN_TEST_LSS,
    SER_TX_CAN_CTL_WARN,
    CAN_MODE_DISCRETE_OUTPUT_OVERRIDE,
    INSPECTION_LIGHT,
    WARN_LIGHT,
} test_list;

void runBoardBringUpTest(int test);


#endif /* INCLUDE_BOARDBRINGUP_H_ */
