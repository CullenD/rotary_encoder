/*
 * common.h
 *
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>


/************************************************************
 * Logging Configuration
 ***********************************************************/
#define PRINT_STATE_IN_CASE                 // uses hard-coded string names and transmits with UART
#define COUNT_SENSOR_ERROR                  // updates global variables within ReadInputs()
#define RECORDS                             // creates structs to record state data
#define TX_STATE_RECORDS                    // transmits last state data with UART
//#define TX_SYSTEM_RECORD_IN_OPEN_STATE
#define PRINT_SWITCH_STATUS_AFTER_INIT

#define SW_VERSION_STRING "v1.23"
#define SW_VERSION_NAME_STRING "(TT Prototype - Apr 20 2018)"


/************************************************************
 * I/O Configuration
 ***********************************************************/
#define WO_TYPE_HSS TRUE    // Warning output type (high side switch or low side switch)
#define CO_TYPE_HSS TRUE    // Couple output type (high side switch or low side switch)
#define CAN_MODE    FALSE   // Can mode enabled? If true, disables discrete CPL and WARN outputs.


/************************************************************
 * Custom Data Types
 ***********************************************************/
// Boolean Macros
#define TRUE 1
#define FALSE 0

typedef struct //systemRecord_t
{
	uint16_t tag;                                  //  1 word address
	uint16_t sysTime_ms;                           //  1 word address

	uint32_t sysTime_min;                          //  2 word address
                                                        //     4
	uint32_t countINIT_STATE;			            //  2 word address
	uint32_t countOPEN_STATE;			            //  2 word address
	uint32_t countINDETERMINATE_STATE;             //  2 word address
	uint32_t countINSPECTION_STATE;                //  2 word address
	uint32_t countLOCK_MONITORING_STATE;           //  2 word address
	uint32_t countUNLOCKED_STATE;                  //  2 word address
	uint32_t countERROR_STATE;	                    //  2 word address
                                                        //    14
	uint32_t elapsedMinutesINIT_STATE;			    //  2 word address
	uint32_t elapsedMinutesOPEN_STATE;			    //  2 word address
	uint32_t elapsedMinutesINDETERMINATE_STATE;    //  2 word address
	uint32_t elapsedMinutesINSPECTION_STATE;       //  2 word address
	uint32_t elapsedMinutesLOCK_MONITORING_STATE;  //  2 word address
	uint32_t elapsedMinutesUNLOCKED_STATE;         //  2 word address
	uint32_t elapsedMinutesERROR_STATE;	           //  2 word address
                                                        //    14
	uint32_t totalErrOverflowFlags;                //  2 word address
	uint32_t totalKpsErr_ms;                      //  2 word address
	uint32_t totalloadSwitchState;                      //  2 word address
	uint32_t totalLksErr_ms;                       //  2 word address
	                                                    //     8
} systemRecord_t;

typedef struct //stateRecord_t
{
	uint16_t tag;                  //  1 word address
	uint16_t sysTime_ms;           //  1 word address
	uint32_t sysTime_min;          //  2 word address
	uint32_t kpsErr_ms;            //  2 word address
	uint32_t loadSwitchState;      //  2 word address
	uint32_t lksErr_ms;            //  2 word address
} stateRecord_t;

typedef enum tags_e //tag must not be 0xFFFF, which is used to identify erased flash
{
	INIT_TAG = 10000,
	SYST_TAG = 10101,
	OPEN_TAG = 1111, //0x457
	INDETERMINATE_TAG = 2222,
	INSPECTION_TAG = 3333,
	LOCK_MONITORING_TAG = 4444,
	UNLOCKED_TAG = 5555,
	ERROR_TAG = 6666
} tags_t;

/************************************************************
 * Global Variables
 ***********************************************************/
// TODO: Should be avoided as much as possible
extern systemRecord_t   recordCurrentSystem;

#endif /* COMMON_H_ */
