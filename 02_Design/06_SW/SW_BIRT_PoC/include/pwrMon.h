/*
 * pwrMon.h
 *
 *  Created on: Jan 29, 2018
 *      Author: CollinsBr
 */

#ifndef INCLUDE_PWRMON_H_
#define INCLUDE_PWRMON_H_

// Keep track of input voltage
extern float    inpV;

// Indicates if input power is within a valid range (either TRUE or FALSE). Debounce of 10 samples.
extern uint16_t pwrOK;


/***************************************************************************************
* Sets up the ADC for input voltage sensing.
***************************************************************************************/
void initADC(void);

/***************************************************************************************
* Checks the input voltage and updates pwrOK value (TRUE or FALSE).
*
* If power transitions from valid -> invalid, writes histogram to flash.
***************************************************************************************/
void checkInpVoltage(void);

/***************************************************************************************
* Checks the input voltage stepped from a battery or cranking voltage level to an engine
* running level. Returns 1 if a step detected, 0 otherwise.
***************************************************************************************/
uint16_t ignStepDetected(void);

/***************************************************************************************
* Clears the "ignition stepped" status variable to allow detection of a new step.
***************************************************************************************/
void clearIgnStep(void);


#endif /* INCLUDE_PWRMON_H_ */
