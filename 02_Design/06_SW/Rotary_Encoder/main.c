/*
*Title: Rotary Encoder
*Author: Dylan Cullen
*Description: Software used to determine linear displacement relative to
*             angular displacement of a rotary encoder sensor.
*/
//Necessary includes
#include <msp430.h> 
#include <stdint.h>
#include "printf.h"
#include "uart.h"
//Global definitions for logic
#define TRUE 0
#define FALSE 1
//Function init
int btnPressed();
void edgeDetect();
void checkSensor();
int senseOne();
int senseTwo();
void updatePosition();
//Global variable init
const int senseOneOld = 1;
const int senseOneNew = 1;
int senseOneRise = 0;
int senseOneFall = 0;

int senseOneState = 2;
int senseTwoState = 2;
int state = 42;
double position = 0;
int posdec = 0;
//Check pin status
int senseOne(){
    return !(P2IN&BIT0);
}
int senseTwo(){
    return !(P2IN&BIT1);
}
//Not currently being used
void edgeDetect(){
   if(senseOneOld < senseOneNew){
       senseOneFall = 1;
       senseOneRise = 0;
   }
   if(senseOneNew < senseOneOld){
       senseOneFall = 0;
       senseOneRise = 1;
   }
   if(senseOneNew == senseOneOld)
   {
       senseOneFall = 0;
       senseOneRise = 0;
   }
}


#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void){

}






void checkSensor(){
    //Function used to read and debounce current state of both sensors
    const int countReset = 1;   //Debounce variable that should be changed based on performance
    static int senseOneCount = countReset;
    static int senseTwoCount = countReset;

    if(senseOne()){  //Check if sensor one is active
        if(senseOneState == FALSE){  //Check if we think sensor is not active
            senseOneCount--;    //Decrement count while the sensor is active and we think it is not pressed
        }
        else{   //If sensor one is active and we think it is active
            senseOneCount = countReset;    //Reset count while sensor one is active
        }
    }
    else{   //If sensor one is not active
        if(senseOneState == TRUE){    //If we think the sensor one is active
            senseOneCount--;    //Decrement count while sensor one is not active and we think it is active
        }
        else{   //If we think sensor one is not active
            senseOneCount = countReset;    //If sensor one is not active and we think it is not active reset count
        }
    }
    if(senseOneCount == 0){ //If sensor one is active (or not active) for long enough, toggle pin state accordingly
        senseOneState = !senseOneState;
        senseOneCount = countReset;
    }


    if(senseTwo()){  //Check if sensor Two is active
            if(senseTwoState == FALSE){  //Check if we think sensor Two is not active
                senseTwoCount--;    //Decrement count while the sensor Two is active and we think it is not active
            }
            else{   //If sensor Two is active and we think it is active
                senseTwoCount = countReset;    //Reset count while sensor Two is active
            }
        }
        else{   //If sensor Two is not active
            if(senseTwoState == TRUE){    //If we think the sensor Two is active
                senseTwoCount--;    //Decrement count while sensor Two is not active and we think it is active
            }
            else{   //If we think sensor Two is not active
                senseTwoCount = countReset;    //If sensor Two is not active and we think it is not active reset count
            }
        }
        if(senseTwoCount == 0){ //If sensor Two is active (or not active) for long enough, toggle pin state accordingly
            senseTwoState = !senseTwoState;
            senseTwoCount = countReset;
        }
}

void updatePosition(void){
    //Function that determines if the absolute position has been changed and calculates position accordingly
    int newState = 0;   //dummy variable declaration
    const float length = 0.0148;    //Length variable that should be changed based on overall system
    static int32_t ticks = 0;   //variable to count number of ticks. only used in this function.
    //Series of if statements that determine current state of system
    if((senseOneState == 0) && (senseTwoState == 0)){
        newState = 0;
    }
    if((senseOneState == 1) && (senseTwoState == 0)){
        newState = 1;
    }
    if((senseOneState == 1) && (senseTwoState == 1)){
        newState = 2;
    }
    if((senseOneState == 0) && (senseTwoState == 1)){
        newState = 3;
    }
    //Series of if statements that determine if there has been displacement. If there has been a change, the position
    //will be printed
    if((state == 0) && (newState == 1)){
        ticks--;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 0) && (newState == 3)){
        ticks++;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 1) && (newState == 2)){
        ticks--;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 1) && (newState == 0)){
        ticks++;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 2) && (newState == 3)){
        ticks--;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 2) && (newState == 1)){
        ticks++;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r\n", posdec);
    }
    if((state == 3) && (newState == 0)){
        ticks--;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d", posdec);
    }
    if((state == 3) && (newState == 2)){
        ticks++;
        position = ticks * length;
        posdec = ((int)(position*1000))%1000;
        printf("\rPosition (inches): %d" , (int)position);
        printf(".%d\r", posdec);
    }
    else{
        //not sure so take current state and do not change position
        ticks = ticks;
    }
    state = newState; //Updates state of system
}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// stop watchdog timer
	UartInit();
	init_printf(0,putc);

    P2DIR &= ~BIT0; //Clear P2.0 to set DIR to Input
    P2REN |= BIT0;  //Set P2.0 to enable internal resistor
    P2OUT &= ~BIT0;  //Set P2.0 to set Resistor to pull up

    P2DIR &= ~BIT1; //Clear P2.1 to set DIR to Input
    P2REN |= BIT1;  //Set P2.1 to enable internal resistor
    P2OUT &= ~BIT1;  //Set P2.1 to set Resistor to pull up

    //SET THIS UP AFTER READING IN CURRENT SENSOR STATES
        //It needs to be set to look for rising or falling edge and can only be set for one
        //This process will be repeated in an interrupt.
    //Interrupt enable
    P2DIR &= ~BIT0;         // external power sense
    P2IE |= BIT0;           // enable IO interrupt
    P2IES |= BIT0;          // EdgeSelect = 1: flag sets on falling edge
    P2IFG &= ~BIT0;         // clear interrupt flag


    //initial state of sensor
    senseOneState = senseOne();
    senseTwoState = senseTwo();
    printf("Starting.\r\n");
	while(1){
	    checkSensor();
	    updatePosition();
	}
}
