/*
 * pwrMon.c
 *
 *  Created on: Jan 29, 2018
 *      Author: CollinsBr
 */

#include <msp430g2755.h>
#include <stdint.h>
#include <string.h>
#include "pwrMon.h"
#include "common.h"


const float     minInpV_LH      = 7.1;
const float     minInpV_HL      = 6.9;
const float     pwrOKMinInpV    = 8.0;
const float     pwrOKMaxInpV    = 32.0;

const float     resDivGain      = 13.4;
const float     refVoltage      = 2.5;
const uint16_t  ADCSteps        = 1023;
float           inpV            = 0.0;
uint16_t        pwrOK           = FALSE;

#define PWR_OK_DEBOUNCE_SAMPLES 10


void checkInpVoltage(void)
{
    // Monitor input power. Power sufficient means there is power available that can run the device,
    // power OK means the power is within a normal range.
    static uint16_t pwrSufficient   = FALSE;
    static uint16_t pwrOKSample     = FALSE;
    static uint16_t pwrOKDebounce   = 0;

    // There is a voltage divider of 124k and 10k on input power. We need to multiply the ADC value by this gain (13.4) to turn a 0V-2.5V
    // signal into a 0V - 33.5V signal. The ADC rails above 33.5V (overvoltage shutoff happens between 33.4V and 39.8V - nominal 36.9V)
    const float voltsPerLSB = (resDivGain*refVoltage)/ADCSteps;


    // Wait for conversion to finish
    while(ADC10CTL1 & ADC10BUSY);

    inpV = ADC10MEM * voltsPerLSB;    // 10 bit ADC value. 1023 steps ranging from 0V to 2.5V -> floating point value from 0V to 33.5V.

    // Undervoltage protect.
    if((pwrSufficient == TRUE) && (inpV < minInpV_HL))
    {
        pwrSufficient = FALSE;
    }
    else if((pwrSufficient == FALSE) && (inpV > minInpV_LH))
    {
        pwrSufficient = TRUE;
    }
    else if(pwrSufficient == FALSE)
    {
        pwrSufficient = FALSE;
    }

    // Determine if power is within a valid range, set pwrOKSample accordingly
    if( (inpV > pwrOKMinInpV) && (inpV < pwrOKMaxInpV) )
    {
        pwrOKSample = TRUE;
    }
    else
    {
        pwrOKSample = FALSE;
    }

    // Debounce pwrOKSample to generate pwrOK.
    if (pwrOKSample == pwrOK) // no pending change
    {
        pwrOKDebounce = 0;
    }
    else // pending change
    {
        pwrOKDebounce++;
        if (pwrOKDebounce  >= PWR_OK_DEBOUNCE_SAMPLES) // change state
        {
            pwrOK = !pwrOK;
            pwrOKDebounce = 0;
        }
    }

    ADC10CTL0 |= ADC10SC; // Start next conversion
}

void initADC(void)
{
    // Configure power sense as an analog input
    ADC10AE0 = BIT0;        // Configure power sense pin (P2.0 - A0) as analog input
    ADC10DTC1 = 0x0000;     // Disable automatic data transfers to memory, we don't need them.
    ADC10DTC0 = 0x0000;

    // ****** THIS REGISTER NEEDS TO BE SET BEFORE ADC10CTL1 - the ENC bit in that register prevents writing to parts of this register ******
    // INCH_0 - Select channel 0.
    // ADC10SC bit used as S&H source.
    // Straight binary format.
    // Clock divide by 8.
    // Use SMCLK - 1MHz/(8 * 64) = 1.95 kHz conversion rate.
    // Repeatedly convert single channel.
    ADC10CTL1 = INCH_0 | ADC10DIV0 | ADC10DIV1 | ADC10DIV2 | ADC10SSEL_3;// | CONSEQ1;

    // MSC - Multiple sample and convert (continuously do conversions),
    // SREF0 - Vref positive reference & 0V negative reference,
    // ADC10SHT_3 - 64 clock cycles per sample & hold,
    // Interrupt is not enabled.
    // ENC - Conversion is enabled.
    ADC10CTL0 = MSC | SREF0 | ADC10SHT_3 | REF2_5V | REFON | ADC10ON | ENC;

    ADC10CTL0 |= ADC10SC; // Start conversion
}
