/*
 * sysClock.c
 *
 *  Created on: Apr 19, 2018
 *      Author: CollinsBr
 */

#include <msp430g2755.h>
#include <stdint.h>
#include "sysClock.h"


/************************************************************
 * Timing Parameters
 ***********************************************************/
#define MS_PER_MIN          60000
#define TIMER_COUNT_1MHZ    1000

/************************************************************
 * Clock State
 ***********************************************************/
uint16_t    hardMilliseconds;
uint16_t    sysTick_1ms;
uint32_t    sysTick_1min;

/************************************************************
 * ISRs
 ***********************************************************/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
    ++hardMilliseconds;
}

/************************************************************
 * Function Implementations
 ***********************************************************/
void initClock(void)
{
    // Reset system time
    sysTick_1ms  = 0;
    sysTick_1min = 0;

    // Configure timer for systick
    TA0CCTL0 = CCIE;                // CCR0 interrupt enabled
    TA0CCR0 = TIMER_COUNT_1MHZ;     // 1MHz / 1000 = 1 ms tick
    TA0CTL = TASSEL_2 + MC_1;       // SMCLK, upmode
    __enable_interrupt();
}

void updateClock(void)
{
    sysTick_1ms += hardMilliseconds;
    if (sysTick_1ms >= MS_PER_MIN)
    {
        sysTick_1ms -= MS_PER_MIN;
        ++sysTick_1min;
    }

    hardMilliseconds = 0;
}

uint16_t getSysTickMs(void)
{
    return sysTick_1ms;
}

void setSysTickMs(uint16_t sysTickMs)
{
    sysTick_1ms = sysTickMs;
}

uint32_t getSysTickMin(void)
{
    return sysTick_1min;
}

void setSysTickMin(uint16_t sysTickMin)
{
    sysTick_1min = sysTickMin;
}

uint16_t getHardMilliseconds(void)
{
    return hardMilliseconds;
}
