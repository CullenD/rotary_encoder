################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/boardBringUp.c \
../src/flash.c \
../src/hal.c \
../src/main.c \
../src/printf.c \
../src/pwrMon.c \
../src/sysClock.c \
../src/uart.c 

C_DEPS += \
./src/boardBringUp.d \
./src/flash.d \
./src/hal.d \
./src/main.d \
./src/printf.d \
./src/pwrMon.d \
./src/sysClock.d \
./src/uart.d 

OBJS += \
./src/boardBringUp.obj \
./src/flash.obj \
./src/hal.obj \
./src/main.obj \
./src/printf.obj \
./src/pwrMon.obj \
./src/sysClock.obj \
./src/uart.obj 

OBJS__QUOTED += \
"src\boardBringUp.obj" \
"src\flash.obj" \
"src\hal.obj" \
"src\main.obj" \
"src\printf.obj" \
"src\pwrMon.obj" \
"src\sysClock.obj" \
"src\uart.obj" 

C_DEPS__QUOTED += \
"src\boardBringUp.d" \
"src\flash.d" \
"src\hal.d" \
"src\main.d" \
"src\printf.d" \
"src\pwrMon.d" \
"src\sysClock.d" \
"src\uart.d" 

C_SRCS__QUOTED += \
"../src/boardBringUp.c" \
"../src/flash.c" \
"../src/hal.c" \
"../src/main.c" \
"../src/printf.c" \
"../src/pwrMon.c" \
"../src/sysClock.c" \
"../src/uart.c" 


