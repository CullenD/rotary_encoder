/*
 * flash.h
 *
 *  Created on: Feb 4, 2016
 *      Author: robertvanvossen
 */

#ifndef FLASH_H_
#define FLASH_H_

#include "common.h"

#define FLASH_STORAGE

#ifdef FLASH_STORAGE
// approximate flash life ~10,000 cycles: http://www.ti.com/lit/an/slaa334a/slaa334a.pdf
void SelectEmptyLog();
void Log(stateRecord_t * sr);
#else
#define SelectEmptyLog()
#define Log(sr)
#endif

#endif /* FLASH_H_ */
