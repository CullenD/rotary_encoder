/****************************************************************************************
 *  MSP430
 *
 *  Description: SAF-HOLLAND Coupling Monitor
 *               UART and Serial handling.
 *
 * Andrew Wallner
 *
 ***************************************************************************************/
#include <msp430g2553.h>
#include <stdint.h>
#include <stdlib.h>
#include "uart.h"
#include "printf.h"

#define BAUDRATE_9600 104
#define UART_INIT_DELAY 10000

void putc ( void* p, char c);

//static void UartUlTx(uint32_t val); //long val);
//static void Uint32ToA( char *out, unsigned long v );

char * UiToA (uint16_t value, char *result, int base);

//static uint32_t hours;
//static uint32_t minutes;


void UartInit(void)
{
	uint16_t i;
	P1SEL = BIT2;             	    // Select TX functionality for P1.2
	P1SEL2 = BIT2;
	UCA0CTL1 |= UCSSEL_2;           // Have USCI use System Master Clock (1MHz)
	UCA0BR0 = BAUDRATE_9600;        // 1MHz, 9600
	UCA0BR1 = 0;

	UCA0MCTL = UCBRS0;              // Modulation UCBRSx = 1
	UCA0CTL1 &= ~UCSWRST;           // Start USCI state machine

	// delay to allow (external) serial adaptor to power-up
	for(i=UART_INIT_DELAY;i>0;i--)
	{
		_nop();        		// delay to allow (external) serial adaptor to power-up
	}

	init_printf(NULL,putc);

	//UartTx("\r\n\n\nSAF-HOLLAND Coupling Monitor "SW_VERSION_STRING"\r\n");
	//UartTx("  "SW_VERSION_NAME_STRING"\r\n");
	//printf("\r\n\n\nSAF-HOLLAND Coupling Monitor\r\n");
	//printf("  ASCIIFW\r\n\n");


	//uint32_t t32 = 1234567890;
	//uint16_t t16 = 65432;
	//printf("Testing printf t32:  = %lu", t32);
	//printf("Testing printf t16 :  = %11u \r\n", t16);
	//printf("Testing printf t32 :  = %11lu \r\n", t32);
}

void UartTx(char * tx_data)
{
	uint16_t i=0;
	while(tx_data[i]) // Incrementing through array, look for null pointer (0) at end of string
	{
		while ((UCA0STAT & UCBUSY)); // Wait if line TX/RX module is busy with data
		UCA0TXBUF = tx_data[i]; // Send out element i of tx_data array on UART bus
		i++; // Increment variable for array address
	}
}


void putc ( void* p, char c)
		{
		//while (!SERIAL_PORT_EMPTY) ;
		while ((UCA0STAT & UCBUSY)); // Wait if line TX/RX module is busy with data
		//SERIAL_PORT_TX_REGISTER = c;
		UCA0TXBUF = c; // Send out c on UART bus
		}

#if 0
static void UartUlTx(uint32_t val)
//void UartUlTx(int32_t val) //needed to be static for use in interrupt
{
	char s[11];

	ltoa(val,&s[0]);
	//Uint32ToA(&s[0],val);//used fixed-width output function
	printf("UartUlTx using printf! = %u", val);

	UartTx(&s[0]);
}
#endif

#if 0
static void Uint32ToA( char *out, unsigned long v )
	{
	    uint8_t work[10] = { 0 };
	    int count = 0;
	    int i =0;
	    do {
	      work[count++] = v % 10;
	      v /= 10;
	    } while ( v );
	    for(i=10; i>count;i--) *out++ = ' '; //pad with spaces for fixed-length string output
	    while ( count > 0 )
		  *out++ = '0' + work[--count];
		*out = 0;
	}
#endif


#if 0
void UartHexAddrTx(uint16_t * addr)
{

	printf("UartHexAddrTx using printf! = %x", addr);
	//char s[11];
	//UiToA((uint16_t) addr,&s[0],16);
	//UartTx(&s[0]);
}

char * UiToA (uint16_t value, char *result, int base)
{
    // check that the base if valid
    if (base < 2 || base > 36) { *result = '\0'; return result; }

    char* ptr = result, *ptr1 = result, tmp_char;
    int tmp_value;

    do {
        tmp_value = value;
        value /= base;
        *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
    } while ( value );

    // Apply negative sign
    if (tmp_value < 0) *ptr++ = '-';
    *ptr-- = '\0';
    while (ptr1 < ptr) {
        tmp_char = *ptr;
        *ptr--= *ptr1;
        *ptr1++ = tmp_char;
    }
    return result;
}
#endif

/*
void TxStateRecord(stateRecord_t * sr)
{
	switch(sr->tag)
	{
		case SYST_TAG:
			printf(" SYST ");//UartTx(" SYST: ");
			break;
		case INIT_TAG:
			printf(" INIT ");
			break;
		case OPEN_TAG:
			printf(" OPEN ");
			break;
		case INDETERMINATE_TAG:
			printf(" INDT ");
			break;
		case INSPECTION_TAG:
			printf(" INSP ");
			break;
		case LOCK_MONITORING_TAG:
			printf(" LOCK ");
			break;
		case UNLOCKED_TAG:
			printf(" UNLK ");
			break;
		case ERROR_TAG:
			//if (WARNING_OUTPUT_STATE)
			//{
			//	delayTxFlag = TRUE;
			//}
			printf("\r\r");
			printf(" ERRR ");
			break;
		default:
			printf(" bad state tag: %u",sr->tag);
			//UartUlTx(sr->tag);
			break;
	}

	//>Stored data:
	//> LOG f000:
	//>  STAG: "

	//hours=sr->sysTime_min;
	//hours/=60;
	//minutes=(sr->sysTime_min - (hours*60));

	printf("@ %8lu:%5u  k=%3lu  l=%3lu ",sr->sysTime_min,sr->sysTime_ms,sr->kpsErr_ms,sr->lksErr_ms);
	if(sr->tag == INIT_TAG)
	{
		printf("  ot=%2x\r\n",sr->loadSwitchState );
	}
	else
	{
		printf("\r\n");
	}
	//UartTx("  @min:ms: ");
	//UartUlTx(sr->sysTime_min);
	//UartTx(":");
	//UartUlTx(sr->sysTime_ms);
	//UartTx("  err_ms: kpl: ");
	//UartUlTx(sr->kpsErr_ms);
	//UartTx("  kpr: ");
	//UartUlTx(sr->kpsrErr_ms);
	//UartTx("  lck: ");
	//UartUlTx(sr->lksErr_ms);
	//UartTx("\r\n");
}

void TxSystemRecord(systemRecord_t * sr)
{
	//uint32_t totalMinutes = 0;
	switch(sr->tag)
	{
		case SYST_TAG:
			printf(" SYST ");
			break;
		default:
			printf("  bad tag: %u", sr->tag);
			//UartTx("  bad tag: ");
			//UartUlTx(sr->tag);
			break;
	}

	printf("@ %8lu:%5u\r\n",sr->sysTime_min,sr->sysTime_ms);
	//UartTx("  @min:ms: ");
	//UartUlTx(sr->sysTime_min);
	//UartTx(":");
	//UartUlTx(sr->sysTime_ms);
	//UartTx("\r\n");

	printf("  INIT n=%6lu m=%8lu",sr->countINIT_STATE,sr->elapsedMinutesINIT_STATE);
	printf("    OPEN n=%6lu m=%8lu\r\n",sr->countOPEN_STATE,sr->elapsedMinutesOPEN_STATE);
	printf("  INDT n=%6lu m=%8lu",sr->countINDETERMINATE_STATE,sr->elapsedMinutesINDETERMINATE_STATE);
	printf("    INSP n=%6lu m=%8lu\r\n",sr->countINSPECTION_STATE,sr->elapsedMinutesINSPECTION_STATE);
	printf("  LOCK n=%6lu m=%8lu",sr->countLOCK_MONITORING_STATE,sr->elapsedMinutesLOCK_MONITORING_STATE);
	printf("    UNLK n=%6lu m=%8lu\r\n",sr->countUNLOCKED_STATE,sr->elapsedMinutesUNLOCKED_STATE);
	printf("  ERRR n=%6lu m=%8lu\r\n",sr->countERROR_STATE,sr->elapsedMinutesERROR_STATE);
	printf("  snsr ov=%lu k=%lu  l=%lu\r\n",sr->totalErrOverflowFlags,sr->totalKpsErr_ms,sr->totalLksErr_ms);

	//printf("  INIT count: %6lu min: %8lu\r\n",sr->countINIT_STATE,sr->elapsedMinutesINIT_STATE);
	//printf("  OPEN count: %6lu min: %8lu\r\n",sr->countOPEN_STATE,sr->elapsedMinutesOPEN_STATE);
	//printf("  INDT count: %6lu min: %8lu\r\n",sr->countINDETERMINATE_STATE,sr->elapsedMinutesINDETERMINATE_STATE);
	//printf("  INSP count: %6lu min: %8lu\r\n",sr->countINSPECTION_STATE,sr->elapsedMinutesINSPECTION_STATE);
	//printf("  LOCK count: %6lu min: %8lu\r\n",sr->countLOCK_MONITORING_STATE,sr->elapsedMinutesLOCK_MONITORING_STATE);
	//printf("  UNLK count: %6lu min: %8lu\r\n",sr->countUNLOCKED_STATE,sr->elapsedMinutesUNLOCKED_STATE);
	//printf("  ERRR count: %6lu min: %8lu\r\n",sr->countERROR_STATE,sr->elapsedMinutesERROR_STATE);
	//printf("  sensr: ovr:     %11lu kpms:%11lu  lkms:%11lu \r\n",sr->totalErrOverflowFlags,sr->totalKpsErr_ms,sr->totalLksErr_ms);
//            sensors: overflow:

#if 0
	UartTx("\r\n    INIT    total count: ");
	UartUlTx(sr->countINIT_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesINIT_STATE);

	UartTx("\r\n    OPEN    total count: ");
	UartUlTx(sr->countOPEN_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesOPEN_STATE);

	UartTx("\r\n    INDT    total count: ");
	UartUlTx(sr->countINDETERMINATE_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesINDETERMINATE_STATE);

	UartTx("\r\n    INSP    total count: ");
	UartUlTx(sr->countINSPECTION_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesINSPECTION_STATE);

	UartTx("\r\n    LOCK    total count: ");
	UartUlTx(sr->countLOCK_MONITORING_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesLOCK_MONITORING_STATE);

	UartTx("\r\n    UNLK    total count: ");
	UartUlTx(sr->countUNLOCKED_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesUNLOCKED_STATE);

	UartTx("\r\n    ERRR    total count: ");
	UartUlTx(sr->countERROR_STATE);
	UartTx("    minutes: ");
	UartUlTx(sr->elapsedMinutesERROR_STATE);

	totalMinutes += sr->elapsedMinutesINIT_STATE +
					sr->elapsedMinutesOPEN_STATE +
					sr->elapsedMinutesINDETERMINATE_STATE +
					sr->elapsedMinutesINSPECTION_STATE +
					sr->elapsedMinutesLOCK_MONITORING_STATE +
					sr->elapsedMinutesUNLOCKED_STATE +
					sr->elapsedMinutesERROR_STATE;


	UartTx("\r\n    System  total running time         minutes: ");
	UartUlTx(totalMinutes);

	UartTx("\r\n    Error over-flow:  ");
	UartUlTx(sr->totalErrOverflowFlags);

	UartTx("\r\n    Total kpl err_ms: ");
	UartUlTx(sr->totalKpsErr_ms);

	UartTx("\r\n    Total kpr err_ms: ");
	UartUlTx(sr->totalKpsrErr_ms);

	UartTx("\r\n    Total lks err_ms: ");
	UartUlTx(sr->totalLksErr_ms);

	UartTx("\r\n");
#endif

}
*/
