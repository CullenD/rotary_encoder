/****************************************************************************************
 *  MSP430
 *
 *  Description: SAF-HOLLAND Coupling Monitor
 *               Flash handling.
 *
 * Andrew Wallner
 *
 ***************************************************************************************/
#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>

#include "flash.h"
#include "uart.h"
#include "printf.h"

#ifdef FLASH_STORAGE
// Macros
#define LOG_SIZE 0x200
#define UINT16_SIZE_IN_BYTES 2
#define ERASED_FLASH 0xFFFF

// flash log base addresses
#define LOG_END_ADDR (uint16_t *)0xFE00 //CAUTION: This is flash segment 0, which contains interrupt vectors.
#define LOG_START_ADDR (uint16_t *)0xF000//0xF800 //LOG 1

// Variables
uint16_t * logAddr = LOG_START_ADDR; // flash circular buffer index (next erased address)
uint16_t * nextLogAddr = LOG_START_ADDR; // beginning of next erased flash circular buffer segment

// Function Prototypes
static void FlashErase(uint16_t *addr);
static uint16_t * FlashWrite(uint16_t *destaddr, uint16_t *sourceAddr, uint16_t qty);
static void GetLog(uint16_t * a);
static void StartNewLogIfFull(void);

// flash functions based on https://cyroforge.wordpress.com/2013/01/01/using-flash-memory-in-msp430/
static void FlashErase(uint16_t *addr)
{
  _DINT();                             // Disable interrupts. This is important, otherwise,
                                       // a flash operation in progress while interrupt may
                                       // crash the system.
  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL2 = FWKEY + FSSEL_1 + FN3;       // Clk = SMCLK/4
  FCTL1 = FWKEY + ERASE;               // Set Erase bit
  FCTL3 = FWKEY;                       // Clear Lock bit
  *addr = 0;                           // Dummy write to erase Flash segment
  while(BUSY & FCTL3);                 // Check if Flash being used
  FCTL1 = FWKEY;                       // Clear WRT bit
  FCTL3 = FWKEY + LOCK;                // Set LOCK bit
  _EINT();							   // Enable interrupts.
}

static uint16_t * FlashWrite(uint16_t *destaddr, uint16_t *sourceAddr, uint16_t qty)
{
	_DINT();                             // Disable interrupts
	PROBE_FLASH_ACTIVE;
	int i = 0;
	FCTL2 = FWKEY + FSSEL_1 + FN0;       // Clk = SMCLK/4
	FCTL3 = FWKEY;                       // Clear Lock bit
	FCTL1 = FWKEY + WRT;                 // Set WRT bit for write operation
	for (i=0; i< qty; i++)
	{
	  *destaddr++ = *sourceAddr++;       // copy value to flash
	}
    FCTL1 = FWKEY;                        // Clear WRT bit
    FCTL3 = FWKEY + LOCK;                 // Set LOCK bit
    _EINT();
    PROBE_FLASH_IDLE;
    return destaddr;
}

static void GetLog(uint16_t * a)
{
	stateRecord_t tempStateRecord;
	const uint16_t * t = a + LOG_SIZE;//ALW: this might be larger than needed
	uint32_t logLastSysTime_min = 0;
	if (*a != ERASED_FLASH)
	{
		if (*a == SYST_TAG)// system tag
		{
			// Copy the logged state record to the current system record  //this does not check if the log is newer than the current
			memcpy(&recordCurrentSystem, a, sizeof(systemRecord_t));
			logLastSysTime_min = recordCurrentSystem.sysTime_min;
			TxSystemRecord(&recordCurrentSystem);
			a += sizeof(systemRecord_t);
		}

		for( ; a < t; a = a + (sizeof(stateRecord_t)))
		{
			if (*a == ERASED_FLASH)
			{
				break;
			}
			// Copy the state record in FLASH to a temp variable
			memcpy(&tempStateRecord, a, sizeof(stateRecord_t));

			//update record from latest time stamps
			recordCurrentSystem.sysTime_ms = tempStateRecord.sysTime_ms;
			recordCurrentSystem.sysTime_min = tempStateRecord.sysTime_min;

			switch(tempStateRecord.tag)
			{
				case SYST_TAG: //bad
					break;
				case INIT_TAG: //INIT
					recordCurrentSystem.countINIT_STATE++;
					recordCurrentSystem.elapsedMinutesINIT_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case OPEN_TAG:  //OPEN
					recordCurrentSystem.countOPEN_STATE++;
					recordCurrentSystem.elapsedMinutesOPEN_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case INDETERMINATE_TAG:  //INDT
					recordCurrentSystem.countINDETERMINATE_STATE++;
					recordCurrentSystem.elapsedMinutesINDETERMINATE_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case INSPECTION_TAG:  //INSP
					recordCurrentSystem.countINSPECTION_STATE++;
					recordCurrentSystem.elapsedMinutesINSPECTION_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case LOCK_MONITORING_TAG:  //LOCK
					recordCurrentSystem.countLOCK_MONITORING_STATE++;
					recordCurrentSystem.elapsedMinutesLOCK_MONITORING_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case UNLOCKED_TAG:  //UNLK
					recordCurrentSystem.countUNLOCKED_STATE++;
					recordCurrentSystem.elapsedMinutesUNLOCKED_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				case ERROR_TAG:  //ERRR
					recordCurrentSystem.countERROR_STATE++;
					recordCurrentSystem.elapsedMinutesERROR_STATE += (tempStateRecord.sysTime_min - logLastSysTime_min);
					break;
				default:

					break;
			}
			logLastSysTime_min = tempStateRecord.sysTime_min;

			TxStateRecord(&tempStateRecord);
		}
	}
}

void SelectEmptyLog() //run this during UNINIT_STATE
{

	uint16_t * logStartAddr = LOG_START_ADDR;

	//UartTx("\r\n Stored data:\r\n");
	printf("Stored data:\r\n\n");

	//find empty log section in flash, if found, set nextLogAddr
	for(logAddr = LOG_START_ADDR; logAddr < LOG_END_ADDR; logAddr = logAddr + (LOG_SIZE/UINT16_SIZE_IN_BYTES) )
	{
		if (*logAddr == ERASED_FLASH) //assume this entire log section is erased
		{
			//UartTx("\r\n  LOG: ");
			//UartHexAddrTx(logAddr);
			//UartTx(" is empty and will be used next.\r\n");
			printf("LOG %x is empty and will be used next.\r\n", logAddr);
			nextLogAddr = logAddr;
			break;
		}
	}
	//logAddr should now be at the beginning of the empty log or the top (end of the log)
	//if at top, set nextLogAddr to default and erase it
	if(logAddr>=LOG_END_ADDR)
	{
		logAddr = LOG_START_ADDR; //roll over to beginning of log and
		nextLogAddr = LOG_START_ADDR; //handle case of no empty log
		//UartTx("\r\n  No empty logs. ");
		//UartHexAddrTx( logAddr);
		//UartTx(" will be erased and used next.\r\n");
		printf("\r\n No empty logs. %x will be erased and used next.\r\n", logAddr);
		//UartTx("\r\n  LOG TO BE ERASED: "); //print log before erasing it
		//UartHexAddrTx( logAddr);
		//UartTx(" :\r\n");

		printf(" LOG TO BE ERASED: %x :\r\n", logAddr);

		GetLog(logAddr);

		FlashErase(nextLogAddr);
		logAddr = nextLogAddr; //system record will be added after GetLog

	}

	//get log sections in chronological order
	//logAddr should now be at the beginning of the empty log section and not at the end of the log

	//  first, get the oldest sections after the empty log
	//  jump past the empty log
	logAddr =  logAddr + (LOG_SIZE/UINT16_SIZE_IN_BYTES);

	///UartTx("\r\nafter gap logAddr: ");
	///UartHexAddrTx( logAddr);
	///UartTx("\r\n");

	for(logStartAddr = logAddr; logStartAddr < LOG_END_ADDR; logStartAddr = logStartAddr + (LOG_SIZE/UINT16_SIZE_IN_BYTES) )
	{
		logAddr=logStartAddr;
		//UartTx("\r\n  LOG: ");
		//UartHexAddrTx( logAddr);
		//UartTx(" :\r\n");
		printf("LOG %x :\r\n", logAddr);

		GetLog(logAddr);//modifies logAddr!
	}
	//  second, get the sections before the empty log
	///UartTx("\r\npre-gap logAddr: ");
	///	UartHexAddrTx( logAddr);
	///	UartTx("\r\n");
	for(logAddr = LOG_START_ADDR; logAddr < nextLogAddr; logAddr = logAddr + (LOG_SIZE/UINT16_SIZE_IN_BYTES) )
	{
		//UartTx("\r\n  LOG: ");
		//UartHexAddrTx( logAddr);
		//UartTx(" :\r\n");
		printf("LOG %x :\r\n", logAddr);
		GetLog(logAddr);
	}
	///UartTx("\r\nabout to move logAddr to nextLogAddr\r\n");
	//move logAddr to the empty log
	logAddr = nextLogAddr;

	///UartTx("\r\nabout to StartNewLogIfFull \r\n");

	//write header to new log and clear next log
	StartNewLogIfFull(); //write header to new log and clear next log

	///UartTx("\r\n  logAddr: ");
	///UartHexAddrTx( logAddr);
	///UartTx("\r\n");

	//UartTx("\r\n End of stored data. \r\n\n");
	printf("\r\nEnd of stored data. \r\n\n");
}

void Log(stateRecord_t * sr)
{
	logAddr = FlashWrite(logAddr, (uint16_t *)sr, sizeof(stateRecord_t));
	StartNewLogIfFull();

}

void StartNewLogIfFull(void)
{
	if ((logAddr + sizeof(stateRecord_t)) >= nextLogAddr) //log section is full
	{
		logAddr = nextLogAddr;

		logAddr = FlashWrite(logAddr, (uint16_t *)&recordCurrentSystem, sizeof(systemRecord_t));

		nextLogAddr = nextLogAddr + (LOG_SIZE/UINT16_SIZE_IN_BYTES);
		if (nextLogAddr >= LOG_END_ADDR) //make circular buffer
		{
			nextLogAddr = LOG_START_ADDR;
		}
		FlashErase(nextLogAddr);
	}

}
#endif
