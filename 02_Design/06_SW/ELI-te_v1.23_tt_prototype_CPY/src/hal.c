/*
 * hal.c
 *
 *  Created on: Apr 12, 2018
 *      Author: CollinsBr
 */

#include <msp430g2755.h>
#include <stdint.h>
#include "hal.h"
#include "pwrMon.h"
#include "sysClock.h"
#include "uart.h"


/************************************************************
 * I/O pins definition
 ***********************************************************/
// Port 1
#define B_CTL_BIT                   BIT7
#define G_CTL_BIT                   BIT6
#define IL_BIT                      BIT6    // G_CTL also controls the inspection lamps
#define P12V_LOW_POWER_MODE_BIT     BIT5    // 12V switching regulator disable
#define SS_EN_BIT                   BIT4    // Switching regulator spread-spectrum enable bit
#define CAN_MODE_BIT                BIT1

// Port 2
#define ACCEL_CS_BIT        BIT2
#define PWR_SENSE_BIT       BIT0

// Port 3
#define SER_RX_BIT      BIT5
#define SER_TX_BIT      BIT4
#define R_CTL_BIT       BIT4    // R_CTL and SER_TX signals use the same pin to allow UART data to be transmitted via the LED. This bit also
#define WL_BIT          BIT4    // activates the warning lamps.
#define SPC_BIT         BIT3
#define SDO_BIT         BIT2
#define SDI_BIT         BIT1
#define CAN_CS_N_BIT    BIT0

// Port 4
#define LKS_BIT         BIT6
#define KPS_BIT         BIT5
#define WO_BIT          BIT4
#define CO_BIT          BIT3
#define CO_TYPE_BIT     BIT2
#define WO_TYPE_BIT     BIT1


/************************************************************
 * Timing Parameters
 ***********************************************************/
#define MIN_VALID_LOCK_SENSOR_INPUT_TIME_MS 200     // glitch avoidance (debounce) for cam sensor
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_MS 200  // glitch avoidance (debounce) for kingpin sensor

uint16_t minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_MS;

/************************************************************
 * Persistent Data
 ***********************************************************/
uint16_t kp = 0;        //combined kingpin state variable (legacy of single-kingpin sensor system)
uint16_t kps = 0;       //kingpin sensor
uint16_t lks = 0;       //lock sensor

uint16_t kpsDebounce_ms = 0;
uint16_t lksDebounce_ms = 0;

uint16_t kpTimeAsserted_ms = 0;     //timestamp of assertion; elapsed time must be calculated
uint16_t lksTimeAsserted_ms = 0;

#ifdef COUNT_SENSOR_ERROR
uint16_t kpsErr_ms = 0;
uint16_t lksErr_ms = 0;
#endif


/************************************************************
 * Function Implementations
 ***********************************************************/
void p12VHighPowerMode(void)
{
    P1DIR |= P12V_LOW_POWER_MODE_BIT;     // configure P12V disable output (NOTE: one of either P12V or P18V must always be enabled!)
    P1OUT &= ~P12V_LOW_POWER_MODE_BIT;    // 12V regulator enabled
}

void p12VLowPowerMode(void)
{
    P1DIR |= P12V_LOW_POWER_MODE_BIT;     // configure P12V disable output (NOTE: one of either P12V or P18V must always be enabled!)
    P1OUT |= P12V_LOW_POWER_MODE_BIT;     // 12V regulator disabled
}

void cplSelectHSS(void)
{
    P4DIR |= CO_TYPE_BIT;   // configure COUPLE TYPE output (1 = high side switch, 0 = low side switch)
    P4OUT |= CO_TYPE_BIT;   // set output high (high side switch mode)
}

void cplSelectLSS(void)
{
    P4DIR |= CO_TYPE_BIT;   // configure COUPLE TYPE output (1 = high side switch, 0 = low side switch)
    P4OUT &= ~CO_TYPE_BIT;  // set output low (low side switch mode)
}

void cplSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P4OUT &= ~CO_BIT;       // set output low (COUPLE output off)
    }
    else if(state == ACTIVE)
    {
        P4OUT |= CO_BIT;       // set output high (COUPLE output on)
    }
}

void cplOutputDisable(void)
{
    P4DIR &= ~CO_BIT;
}

void cplOutputEnable(void)
{
    P4DIR |= CO_BIT;
    P4DIR &= ~CO_BIT;
}

void warnSelectHSS(void)
{
    P4DIR |= WO_TYPE_BIT;   // configure COUPLE TYPE output (1 = high side switch, 0 = low side switch)
    P4OUT |= WO_TYPE_BIT;   // set output high (high side switch mode)
}

void warnSelectLSS(void)
{
    P4DIR |= WO_TYPE_BIT;   // configure COUPLE TYPE output (1 = high side switch, 0 = low side switch)
    P4OUT &= ~WO_TYPE_BIT;  // set output low (low side switch mode)
}

void warnSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P4OUT &= ~WO_BIT;       // set output low (WARN output off)
    }
    else if(state == ACTIVE)
    {
        P4OUT |= WO_BIT;       // set output high (WARN output on)
    }
}

void warnOutputDisable(void)
{
    P4DIR &= ~WO_BIT;
}

void warnOutputEnable(void)
{
    P4DIR |= WO_BIT;
    P4DIR &= ~WO_BIT;
}

void redLEDEnable(void)
{
    P3DIR |= R_CTL_BIT;     // configure status LED red control output
    P3OUT &= ~R_CTL_BIT;    // set output low (LED off)
}

void redLEDDisable(void)
{
    P3DIR &= ~R_CTL_BIT;     // configure status LED red control as input
}

void redLEDSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P3OUT &= ~R_CTL_BIT;       // set output low (LED off)
    }
    else if(state == ACTIVE)
    {
        P3OUT |= R_CTL_BIT;       // set output high (LED on)
    }
}

void greenLEDEnable(void)
{
    P1DIR |= G_CTL_BIT;     // configure status LED green control output
    P1OUT &= ~G_CTL_BIT;    // set output low (LED off)
}

void greenLEDDisable(void)
{
    P1DIR &= ~G_CTL_BIT;     // configure status LED green control as input
}

void greenLEDSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P1OUT &= ~G_CTL_BIT;       // set output low (LED off)
    }
    else if(state == ACTIVE)
    {
        P1OUT |= G_CTL_BIT;       // set output high (LED on)
    }
}

void blueLEDEnable(void)
{
    P1DIR |= B_CTL_BIT;     // configure status LED blue control output
    P1OUT &= ~B_CTL_BIT;    // set output low (LED off)
}

void blueLEDDisable(void)
{
    P1DIR &= ~B_CTL_BIT;     // configure status LED blue control as input
}

void blueLEDSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P1OUT &= ~B_CTL_BIT;       // set output low (LED off)
    }
    else if(state == ACTIVE)
    {
        P3OUT |= B_CTL_BIT;       // set output high (LED on)
    }
}

void canModeEnable(void)
{
    P1DIR |= CAN_MODE_BIT;  // configure can mode select output
    P1OUT |= CAN_MODE_BIT;  // set output high (CAN mode)
}

void canModeDisable(void)
{
    P1DIR |= CAN_MODE_BIT;  // configure can mode select output
    P1OUT &= ~CAN_MODE_BIT; // set output low (not in CAN mode)
}

void spreadSpectrumEnable(void)
{
    P1DIR |= SS_EN_BIT;  // configure spread spectrum control output
    P1OUT |= SS_EN_BIT;  // set output high (spread spectrum enabled)
}

void spreadSpectrumDisable(void)
{
    P1DIR |= SS_EN_BIT;  // configure spread spectrum control output
    P1OUT &= ~SS_EN_BIT; // set output low (spread spectrum disabled)
}

void inspLightEnable(void)
{
    P1DIR |= IL_BIT;  // configure inspection light control output
    P1OUT &= ~IL_BIT; // set output low (light off)
}

void inspLightDisable(void)
{
    P1DIR &= ~IL_BIT;  // configure inspection light as input
}

void inspLightSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P1OUT &= ~IL_BIT;       // set output low (inspection light off)
    }
    else if(state == ACTIVE)
    {
        P1OUT |= IL_BIT;       // set output high (inspection light on)
    }
}

void warnLightEnable(void)
{
    P3DIR |= WL_BIT;  // configure inspection light control output
    P3OUT &= ~WL_BIT; // set output low (light off)
}

void warnLightDisable(void)
{
    P3DIR &= ~WL_BIT;  // configure inspection light control output
}

void warnLightSetState(uint16_t state)
{
    if(state == INACTIVE)
    {
        P3OUT &= ~WL_BIT;       // set output low (WARN lamp off)
    }
    else if(state == ACTIVE)
    {
        P3OUT |= WL_BIT;       // set output high (WARN lamp on)
    }
}

void uartEnable(void)
{
    // Select TX functionality for P3.4
    P3SEL |= SER_TX_BIT;

    // Select RX functionality for P3.5
    P3SEL |= SER_RX_BIT;
}

void uartDisable(void)
{
    // Deselect TX functionality for P3.4
    P3SEL &= ~SER_TX_BIT;

    // Deselect RX functionality for P3.5
    P3SEL &= ~SER_RX_BIT;
}

void readInputs(void)
{
    // read kingpin sensor input, left
    if (KPS_IS_ACTIVE == kps) //no pending change
    {
#ifdef COUNT_SENSOR_ERROR
        if (kpsDebounce_ms > 0)
        {
            kpsErr_ms = kpsDebounce_ms;// old Debounce_ms is error data: sensor assertion that did not result in a change of state
        }
#endif
        kpsDebounce_ms = 0;
    }
    else //pending change
    {
        kpsDebounce_ms++;
        if (kpsDebounce_ms >= minValidKpsInputTime) //change state
        {
#ifdef COUNT_SENSOR_ERROR
            kpsDebounce_ms = 0; //clear Debounce to prevent false error record
#endif
            kps = !kps;
            if (kps==TRUE)
            {
                kpTimeAsserted_ms = getSysTickMs();//tracking only combined kp time //kpsTimeAsserted_ms = sysTick_1ms;
            }
        }
    }

    kp = kps; //(kps || kpsr);

    // read lock sensor input
    if (LKS_IS_ACTIVE == lks) //no pending change (active-low signal)
    {
#ifdef COUNT_SENSOR_ERROR
        if (lksDebounce_ms > 0)
        {
            lksErr_ms = lksDebounce_ms;// old kpsrDebounce_ms is error data: sensor assertion that did not result in a change of state
        }
#endif
        lksDebounce_ms = 0;
    }
    else //pending change
    {
        lksDebounce_ms++;
        if (lksDebounce_ms  >= MIN_VALID_LOCK_SENSOR_INPUT_TIME_MS) //change state
        {
#ifdef COUNT_SENSOR_ERROR
            lksDebounce_ms = 0; //clear Debounce to prevent false error record
#endif
            lks = !lks;
            if (lks==TRUE)
            {
                lksTimeAsserted_ms = getSysTickMs();
            }
        }
    }

    // Check the input voltage.
    checkInpVoltage();
}

void setKPDebounceTime(uint16_t debounceTime)
{
    minValidKpsInputTime = debounceTime;
}

void initHAL(void)
{
    // 12V regulator configuration
    p12VHighPowerMode();
    spreadSpectrumEnable();

    // LED configuration
    redLEDDisable();    // Enable this after serial transmission is complete.
    greenLEDEnable();
    blueLEDEnable();

    // CAN configuration
    if(CAN_MODE == TRUE)
    {
        canModeEnable();
    }
    else
    {
        canModeDisable();
    }

    // Configure discrete outputs
    warnOutputDisable();// Enable WARN later, after serial TX has completed.
    cplOutputEnable();

    if(WO_TYPE_HSS == TRUE)
    {
        warnSelectHSS();
    }
    else
    {
        warnSelectLSS();
    }

    if(CO_TYPE_HSS == TRUE)
    {
        cplSelectHSS();
    }
    else
    {
        cplSelectLSS();
    }

    // SPI peripheral configuration
    P2DIR |= ACCEL_CS_BIT;  // configure accel chip select (for SPI operation) output
    P2OUT &= ~ACCEL_CS_BIT; // set output low (accel not selected)

    P3DIR |= CAN_CS_N_BIT;  // configure can controller chip select (for SPI operation) output
    P3OUT |= CAN_CS_N_BIT;  // set output high (CAN controller not selected)

    // Sensor Configuration
    P4DIR &= ~LKS_BIT;      // configure lock sensor input
    P4OUT |= LKS_BIT;       // configure resistor as pull-up
    P4REN |= LKS_BIT;       // enable resistor

    P4DIR &= ~KPS_BIT;      // configure kingpin sensor input
    P4OUT |= KPS_BIT;       // configure resistor as pull-up
    P4REN |= KPS_BIT;       // enable resistor


#ifdef LAMPS
    // Init warning lamp after serial transmit is complete.
    warnLightDisable();
    inspLightEnable();
#endif

    UartInit();
}
