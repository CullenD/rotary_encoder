/*
 * boardBringUp.c
 *
 *  Created on: Mar 22, 2018
 *      Author: CollinsBr
 *
 *
 *      This is a series of tests intended to exercise the various functions of the board, developed in the context of performing board bring-up.
 */

#include <msp430g2755.h>
#include "boardBringUp.h"
#include "common.h"
#include "hal.h"


void resetSystemState(void)
{
    // Stop watchdog timer
    WDTCTL = WDTPW | WDTHOLD;

    // Use calibration values for 1MHz Clock DCO
    DCOCTL  = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL  = CALDCO_1MHZ;

    // Set all I/O as inputs
    P1DIR = 0x00;
    P2DIR = 0x00;
    P3DIR = 0x00;
    P4DIR = 0x00;

    // Disable all pull-up and pull-down resistors
    P1REN = 0x00;
    P2REN = 0x00;
    P3REN = 0x00;
    P4REN = 0x00;
}

void runBoardBringUpTest(int test)
{
    // Put the board into a predefined state before each test
    resetSystemState();

    switch(test){
        case P18V_P12V_REG_POINT:
            p12VHighPowerMode();
            break;
        case P12V_LOW_POWER_MODE:
            p12VLowPowerMode();
            break;
        case SPREAD_SPECTRUM_DISABLE:
            spreadSpectrumDisable();
            break;
        case CPL_TEST_HSS:
            cplSelectHSS();
            cplSetState(ACTIVE);
            break;
        case WARN_TEST_HSS:
            warnSelectHSS();
            warnSetState(ACTIVE);
            break;
        case CPL_TEST_LSS:
            cplSelectLSS();
            cplSetState(ACTIVE);
            break;
        case WARN_TEST_LSS:
            warnSelectLSS();
            warnSetState(ACTIVE);
            break;
        case SER_TX_CAN_CTL_WARN:
            warnSelectHSS();
            warnOutputDisable();
            redLEDEnable();
            redLEDSetState(ACTIVE);
            break;
        case CAN_MODE_DISCRETE_OUTPUT_OVERRIDE:
            warnSelectHSS();
            cplSelectHSS();
            canModeEnable();
            warnSetState(ACTIVE);
            cplSetState(ACTIVE);
            break;
        case INSPECTION_LIGHT:
            inspLightEnable();
            inspLightSetState(ACTIVE);
            break;
        case WARN_LIGHT:
            warnLightEnable();
            warnLightSetState(ACTIVE);
            break;
    }
}
