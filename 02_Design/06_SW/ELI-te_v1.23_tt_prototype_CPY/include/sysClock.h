/*
 * sysClock.h
 *
 *  Created on: Apr 19, 2018
 *      Author: CollinsBr
 */

#ifndef INCLUDE_SYSCLOCK_H_
#define INCLUDE_SYSCLOCK_H_

void    initClock(void);
void    updateClock(void);

uint16_t getSysTickMs(void);
void setSysTickMs(uint16_t sysTickMs);

uint32_t getSysTickMin(void);
void setSysTickMin(uint16_t sysTickMin);

uint16_t getHardMilliseconds(void);

#endif /* INCLUDE_SYSCLOCK_H_ */
