/*
 * common.h
 *
 */

#ifndef COMMON_H_
#define COMMON_H_

#define SW_VERSION_STRING "v1.00"
#define SW_VERSION_NAME_STRING "(Production - Sep 7 2016)"

// Boolean Macros
#define TRUE 1
#define FALSE 0

// Common Structs
typedef struct //systemRecord_t
{
	uint16_t tag;                                  //  1 word address
	uint16_t sysTime_ms;                           //  1 word address

	uint32_t sysTime_min;                          //  2 word address
                                                        //     4
	uint32_t countINIT_STATE;			            //  2 word address
	uint32_t countOPEN_STATE;			            //  2 word address
	uint32_t countINDETERMINATE_STATE;             //  2 word address
	uint32_t countINSPECTION_STATE;                //  2 word address
	uint32_t countLOCK_MONITORING_STATE;           //  2 word address
	uint32_t countUNLOCKED_STATE;                  //  2 word address
	uint32_t countERROR_STATE;	                    //  2 word address
                                                        //    14
	uint32_t elapsedMinutesINIT_STATE;			    //  2 word address
	uint32_t elapsedMinutesOPEN_STATE;			    //  2 word address
	uint32_t elapsedMinutesINDETERMINATE_STATE;    //  2 word address
	uint32_t elapsedMinutesINSPECTION_STATE;       //  2 word address
	uint32_t elapsedMinutesLOCK_MONITORING_STATE;  //  2 word address
	uint32_t elapsedMinutesUNLOCKED_STATE;         //  2 word address
	uint32_t elapsedMinutesERROR_STATE;	           //  2 word address
                                                        //    14
	uint32_t totalErrOverflowFlags;                //  2 word address
	uint32_t totalKpsErr_ms;                      //  2 word address
	uint32_t totalloadSwitchState;                      //  2 word address
	uint32_t totalLksErr_ms;                       //  2 word address
	                                                    //     8
} systemRecord_t;

typedef struct //stateRecord_t
{
	uint16_t tag;                  //  1 word address
	uint16_t sysTime_ms;           //  1 word address
	uint32_t sysTime_min;          //  2 word address
	uint32_t kpsErr_ms;            //  2 word address
	uint32_t loadSwitchState;      //  2 word address
	uint32_t lksErr_ms;            //  2 word address
} stateRecord_t;

typedef enum tags_e //tag must not be 0xFFFF, which is used to identify erased flash
{
	INIT_TAG = 10000,
	SYST_TAG = 10101,
	OPEN_TAG = 1111, //0x457
	INDETERMINATE_TAG = 2222,
	INSPECTION_TAG = 3333,
	LOCK_MONITORING_TAG = 4444,
	UNLOCKED_TAG = 5555,
	ERROR_TAG = 6666
} tags_t;

// Globals
// TODO: Should be avoided as much as possible
extern uint16_t blinkTimer;
extern uint16_t lostMilliseconds;
extern uint16_t sysTick_1ms;
extern uint32_t sysTick_1min;
extern uint16_t delayTxFlag;
extern systemRecord_t recordCurrentSystem;

// Common Macros
// TODO: Some of these may not need to be common


#define WARNING_LAMP_TXENA   P2DIR &= ~BIT1
#define WARNING_LAMP_INIT	 P2DIR |= BIT1
#define WARNING_LAMP_OFF     P2OUT &= ~BIT1
#define WARNING_LAMP_ON      P2OUT |= BIT1
#define WARNING_LAMP_FLIP    P2OUT ^= BIT1

#define INSPECTION_LAMP_INIT P1DIR |= BIT3
#define INSPECTION_LAMP_OFF  P1OUT &= ~BIT3
#define INSPECTION_LAMP_ON   P1OUT |= BIT3

#define WARNING_OUTPUT_INIT  P1DIR |= BIT6
#define WARNING_OUTPUT_OFF   P1OUT &= ~BIT6
#define WARNING_OUTPUT_ON    P1OUT |= BIT6
#define WARNING_OUTPUT_STATE ((P1OUT & BIT6) == BIT6)
#define WARNING_OUTPUT_FLIP  P1OUT ^= BIT6

#define COUPLE_OUTPUT_INIT   P1DIR |= BIT5
#define COUPLE_OUTPUT_OFF    P1OUT &= ~BIT5
#define COUPLE_OUTPUT_ON     P1OUT |= BIT5

#define PROBE_FLASH_INIT
#define PROBE_FLASH_ACTIVE	 P2OUT |= BIT6
#define PROBE_FLASH_IDLE	 P2OUT &= ~BIT6

#define LOAD_SWITCH_STATE_WO ((P2IN & BIT5) == BIT5) //low if output is on and shorted to ground
#define LOAD_SWITCH_STATE_CO ((P2IN & BIT0) == BIT0) //low if output is on and shorted to ground
#define LOAD_SWITCH_STATE_WL ((P2IN & BIT2) == BIT2) //low if output is on and shorted to ground
#define LOAD_SWITCH_STATE_IL ((P1IN & BIT4) == BIT4) //low if output is on and shorted to ground

#define ST_SHORT_WO BIT0
#define ST_SHORT_CO BIT1
#define ST_SHORT_WL BIT2
#define ST_SHORT_IL BIT3

#define ST_OPEN_WO BIT4
#define ST_OPEN_CO BIT5
#define ST_OPEN_WL BIT6
#define ST_OPEN_IL BIT7


#endif /* COMMON_H_ */
