//***************************************************************************************
//  MSP430
//
//  Description: SAF-HOLLAND Coupling Monitor (ELI Gen II) PCB EE-11979-B
//               Functional program with serial output and flash storage.
//
//  Andrew Wallner
//
//***************************************************************************************

#define PRINT_STATE_IN_CASE  //uses hard-coded string names and transmits with UART
#define COUNT_SENSOR_ERROR //updates global variables within ReadInputs()
#define RECORDS //creates structs to record state data
#define TX_STATE_RECORDS //transmits last state data with UART
//#define TX_SYSTEM_RECORD_IN_OPEN_STATE
#define PRINT_SWITCH_STATUS_AFTER_INIT

//#include <msp430.h>

#include <msp430g2553.h>
#include <stdint.h>
#include <string.h>
#include "printf.h"
#include "uart.h"
#include "common.h"
#include "flash.h"

//application parameters
#define INIT_TIME_MS 2000                          // time lamps are asserted at power-up (after report from flash)
#define REINIT_TIME_MS 12
#define OUTPUT_INIT_TIME_MS 10                     // time outputs are asserted at power-up (after report from flash)
#define LOAD_SWITCH_OVER_CURRENT_TEST_TIME_MS 0    // time after which load switch is tested for over current (must be 0 since OC protection signal asserts in 50 us)
#define LOAD_SWITCH_OVER_CURRENT_TEST_TIME_NOPS 50 // no-ops after which load switch is tested for over current (OC protection signal asserts in 50 us)
#define LOAD_SWITCH_OPEN_CIRCUIT_TEST_TIME_MS 5    // time after which load switch is tested for open circuitcurrent (open circuit signals asserts in 700 us)
#define MAX_INSPECTION_LAMP_TIME_MINUTES 5         // inspection lamp time-out
#define MAX_LKS_DELAY_TIME_MS 10000                // slow-lock time limit
#define MAX_LKS_EARLY_TIME_MS 1000				   // slow kp flag time limit ***W-M rev1
#define BLINK_HALF_PERIOD_MS 250                   // sets warning lamp flash rate
#define MIN_VALID_LOCK_SENSOR_INPUT_TIME_MS 200    // glitch avoidance (debounce) for cam sensor
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS 200     // glitch avoidance (debounce) for kp sensor before coupling
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS 1000  // glitch avoidance (during travel over curbs and railroad tracks, etc.) for kp sensor
#define MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_INSPECT_MS 200  // (MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS / 8)
#define MAX_TIME_AFTER_UNLOCK_BEFORE_ALARM_MS 3000               // time before warning output is asserted while in unlock state. 65000 milli-seconds max
#define MAX_ERROR_TIME_MINUTES 5                                  // time in error state before state machine is automatically re-initialized ***W-M rev1
#define TIMER_COUNT_1MHZ 1000
#define MS_PER_MIN 60000
#define NOPS_AT_POWER_DOWN 100000 //1000000 // wait about 1 second if power loss is detected before attempting to reinitialize

#define kps_OVERFLOW_FLAG (1 << 0)
#define KPSR_OVERFLOW_FLAG (1 << 1)
#define LKS_OVERFLOW_FLAG (1 << 2)

#define LKS_BIT BIT4
#define KPS_BIT BIT3

// Sensors have an active-low signal so we invert the reading
#define LKS_IS_ACTIVE (!(P2IN & LKS_BIT))
#define KPS_IS_ACTIVE (!(P2IN & KPS_BIT))

//TODO:
#define MAX_INT_VAL 0xFFFFFFFF //check where this is used. Many ints are 16b !

// Enums
// States
// note: Other structures are dependent on the order and quantity of this enum.
typedef enum
{
	UNINIT_STATE,           //notes:
	INIT_STATE,				//print from flash
	OPEN_STATE,				//write to flash
	INDETERMINATE_STATE,
	INSPECTION_STATE,
	LOCK_MONITORING_STATE,	//write to flash
	UNLOCKED_STATE,
	ERROR_STATE				//write to flash
}
states_t;

// Function Prototypes
//hardware support
static void UpdateClock(void);
static void ReadInputs(void);
static void ActivateWarningOutputs(void);
static void Init(void);

#ifdef RECORDS
static void UpdateStateRecord(stateRecord_t *);
static void UpdateRecordsAndLogState(systemRecord_t * sr, states_t lastState);
#endif

// FSM functions
static void HandleUninitState(uint8_t stateChanged);
static void HandleInitState(uint8_t stateChanged);
static void HandleOpenState(uint8_t stateChanged);
static void HandleIndeterminateState(uint8_t stateChanged);
static void HandleInspectionState(uint8_t stateChanged);
static void HandleLockMonitoringState(uint8_t stateChanged);
static void HandleUnlockedState(uint8_t stateChanged);
static void HandleErrorState(uint8_t stateChanged);
static void UpdateState(void);


// Variables (Global and Local)
//system
volatile uint16_t hardMilliseconds = 0;
uint16_t lostMilliseconds = 0;
uint16_t sysTick_1ms = 0;  //force roll at 60000
uint32_t sysTick_1min = 0; //roll at 32b

uint16_t kp = 0;		//combined kingpin state variable (legacy of single-kingpin sensor system)
uint16_t kps = 0;		//kingpin sensor
uint16_t lks = 0;		//lock sensor

uint16_t minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;
uint16_t kpsDebounce_ms = 0;
uint16_t lksDebounce_ms = 0;

uint16_t kpTimeAsserted_ms = 0; 	//timestamp of assertion; elapsed time must be calculated
uint16_t lksTimeAsserted_ms = 0;

uint16_t blinkTimer = BLINK_HALF_PERIOD_MS;

//application support
uint16_t initTimer = INIT_TIME_MS;
uint16_t initTimerMaxValue = INIT_TIME_MS;
uint16_t coupleCount = 0;
uint16_t errorCount = 0;
uint32_t timeCoupled_min = 0;
uint16_t unlockedTime_ms = 0;
uint32_t timeError_min = 0;
//uint16_t delayTxFlag = 0;

states_t state = UNINIT_STATE;
states_t lastState = UNINIT_STATE;

uint16_t loadSwitchState =  0xFFFF; //ST_WO | ST_CO | ST_WL | ST_IL;

#ifdef COUNT_SENSOR_ERROR
uint16_t kpsErr_ms = 0;
uint16_t lksErr_ms = 0;
#endif

#ifdef RECORDS

systemRecord_t recordCurrentSystem =
{
	.tag = SYST_TAG,
	.sysTime_ms = 0,                      
	.sysTime_min = 0,                     
    
	.countINIT_STATE = 0,			      
	.countOPEN_STATE = 0,			      
	.countINDETERMINATE_STATE = 0,        
	.countINSPECTION_STATE = 0,           
	.countLOCK_MONITORING_STATE = 0,      
	.countUNLOCKED_STATE = 0,            
	.countERROR_STATE = 0,	              
    
	.elapsedMinutesINIT_STATE = 0,		 
	.elapsedMinutesOPEN_STATE = 0,		 
	.elapsedMinutesINDETERMINATE_STATE = 0,  
	.elapsedMinutesINSPECTION_STATE = 0,     
	.elapsedMinutesLOCK_MONITORING_STATE = 0,
	.elapsedMinutesUNLOCKED_STATE = 0,       
	.elapsedMinutesERROR_STATE = 0,	        
    
	.totalErrOverflowFlags = 0,              
	.totalKpsErr_ms = 0,
	.totalloadSwitchState = 0,
	.totalLksErr_ms = 0,                     
};

stateRecord_t recordINIT_STATE;
stateRecord_t recordOPEN_STATE;
stateRecord_t recordINDETERMINATE_STATE;
stateRecord_t recordINSPECTION_STATE;
stateRecord_t recordLOCK_MONITORING_STATE;
stateRecord_t recordUNLOCKED_STATE;
stateRecord_t recordERROR_STATE;

static void UpdateStateRecord(stateRecord_t * sr)
{
	// run this function on lastState at state change
	sr->sysTime_ms  = sysTick_1ms;      // timestamp at state exit, when err values are known	
	sr->sysTime_min = sysTick_1min;     // timestamp at state exit, when err values are known
	sr->kpsErr_ms  = kpsErr_ms;
	sr->lksErr_ms   = lksErr_ms;
	sr->loadSwitchState = loadSwitchState;
}

static void UpdateRecordsAndLogState(systemRecord_t * sr, states_t lastState)
{
	// this function is to be run at state change
	
	uint16_t elapsedMinutes = 0; // For now, milliseconds are not counted.

    // compute elapsed time since last state change.
	
	if (sysTick_1min > sr->sysTime_min)
	{
		elapsedMinutes = (sysTick_1min - sr->sysTime_min);
	}
	else
	{
		elapsedMinutes = 0;
	}
 	
	sr->sysTime_min = sysTick_1min;     // timestamp at state exit
	sr->sysTime_ms  = sysTick_1ms;      // timestamp at state exit
    
	
	switch(lastState)
	{
		case INIT_STATE:
			sr->countINIT_STATE++;			          
			sr->elapsedMinutesINIT_STATE += elapsedMinutes;
			UpdateStateRecord(&recordINIT_STATE);
			Log(&recordINIT_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINIT_STATE);
#endif
			break;
		case OPEN_STATE:
			sr->countOPEN_STATE++;
			sr->elapsedMinutesOPEN_STATE += elapsedMinutes;
			UpdateStateRecord(&recordOPEN_STATE);
			Log(&recordOPEN_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordOPEN_STATE);
#endif
			break;
		case INDETERMINATE_STATE:
			sr->countINDETERMINATE_STATE++;            
			sr->elapsedMinutesINDETERMINATE_STATE += elapsedMinutes;
			UpdateStateRecord(&recordINDETERMINATE_STATE);
			Log(&recordINDETERMINATE_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINDETERMINATE_STATE);
#endif
			break;
		case INSPECTION_STATE:
			sr->countINSPECTION_STATE++;
			sr->elapsedMinutesINSPECTION_STATE += elapsedMinutes;
			UpdateStateRecord(&recordINSPECTION_STATE);
			Log(&recordINSPECTION_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordINSPECTION_STATE);
#endif
			break;
		case LOCK_MONITORING_STATE:	
			sr->countLOCK_MONITORING_STATE++;          
			sr->elapsedMinutesLOCK_MONITORING_STATE += elapsedMinutes;
			UpdateStateRecord(&recordLOCK_MONITORING_STATE);
			Log(&recordLOCK_MONITORING_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordLOCK_MONITORING_STATE);
#endif
			break;
		case UNLOCKED_STATE:
			sr->countUNLOCKED_STATE++;                 
			sr->elapsedMinutesUNLOCKED_STATE += elapsedMinutes;		
			UpdateStateRecord(&recordUNLOCKED_STATE);
			Log(&recordUNLOCKED_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordUNLOCKED_STATE);
#endif
			break;
		case ERROR_STATE:
			sr->countERROR_STATE++;	                
			sr->elapsedMinutesERROR_STATE += elapsedMinutes;			
			UpdateStateRecord(&recordERROR_STATE);
			Log(&recordERROR_STATE);
#ifdef TX_STATE_RECORDS
			TxStateRecord(&recordERROR_STATE);
#endif
			break;		
		default:
			break;
	}

	
	if (kpsErr_ms > (MAX_INT_VAL - sr->totalKpsErr_ms)) sr->totalErrOverflowFlags |= kps_OVERFLOW_FLAG; //set overflow flag

	if (lksErr_ms > (MAX_INT_VAL - sr->totalLksErr_ms))  sr->totalErrOverflowFlags |= LKS_OVERFLOW_FLAG;
	
	sr->totalKpsErr_ms += (uint32_t) kpsErr_ms;       //  uint32_t

	sr->totalLksErr_ms  += (uint32_t) lksErr_ms;        //  uint32_t
	
	kpsErr_ms = 0;

	lksErr_ms = 0;		

}

#endif //RECORDS

static void Init(void)
{
	WDTCTL = WDTPW | WDTHOLD;		// Stop watchdog timer

	// Use calibration values for 1MHz Clock DCO
	DCOCTL  = 0;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;

	// Zero out records
#ifdef RECORDS
	recordINIT_STATE = 				(stateRecord_t){INIT_TAG, 0, 0 ,0, 0, 0};
	recordOPEN_STATE = 				(stateRecord_t){OPEN_TAG, 0, 0 ,0, 0, 0};
	recordINDETERMINATE_STATE = 	(stateRecord_t){INDETERMINATE_TAG, 0, 0 ,0, 0, 0};
	recordINSPECTION_STATE = 		(stateRecord_t){INSPECTION_TAG, 0, 0 ,0, 0, 0};
	recordLOCK_MONITORING_STATE = 	(stateRecord_t){LOCK_MONITORING_TAG, 0, 0 ,0, 0, 0};
	recordUNLOCKED_STATE = 			(stateRecord_t){UNLOCKED_TAG, 0, 0 ,0, 0, 0};
	recordERROR_STATE = 			(stateRecord_t){ERROR_TAG, 0, 0 ,0, 0, 0};
#endif

	// configure I/O

	WARNING_LAMP_TXENA;
	//WARNING_LAMP_INIT; //do this later to permit serial TX first
 	INSPECTION_LAMP_INIT;
 	WARNING_OUTPUT_INIT;
 	COUPLE_OUTPUT_INIT;
 	PROBE_FLASH_INIT;


	PROBE_FLASH_IDLE;
	//WARNING_LAMP_OFF; //do this later to permit serial TX first
	INSPECTION_LAMP_OFF;
	WARNING_OUTPUT_OFF;
	COUPLE_OUTPUT_OFF;


	P1DIR &= ~BIT0;         // external power sense
	P1IE |= BIT0;			// enable IO interrupt
	P1IES |= BIT0;          // EdgeSelect = 1: flag sets on falling edge
	P1IFG &= ~BIT0;         // clear interrupt flag

	P2DIR &= ~LKS_BIT;      // configure lock sensor input
	P2OUT |= LKS_BIT;		// configure resistor as pull-up
	P2REN |= LKS_BIT;		// enable resistor

	P2DIR &= ~KPS_BIT;      // kingpin sensor
	P2OUT |= KPS_BIT;		// configure resistor as pull-up
	P2REN |= KPS_BIT;		// enable resistor

	P2DIR &= ~BIT5;         // WO output switch status
	P2OUT |= BIT5;		    // configure resistor as pull-up
	P2REN |= BIT5;		    // enable resistor

	P2DIR &= ~BIT0;         // CO output switch status
	P2OUT |= BIT0;		    // configure resistor as pull-up
	P2REN |= BIT0;		    // enable resistor

	P2DIR &= ~BIT2;         // WL output switch status
	P2OUT |= BIT2;		    // configure resistor as pull-up
	P2REN |= BIT2;		    // enable resistor

	P1DIR &= ~BIT4;         // IL output switch status
	P1OUT |= BIT4;		    // configure resistor as pull-up
	P1REN |= BIT4;		    // enable resistor



	UartInit();

	// configure timer for systick
	TA0CCTL0 = CCIE;                // CCR0 interrupt enabled
	TA0CCR0 = TIMER_COUNT_1MHZ;	 	// 1MHz / 1000 = 1 ms tick
	TA0CTL = TASSEL_2 + MC_1;       // SMCLK, upmode
	__enable_interrupt();

}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
int main(void)
{
	Init();

	while(1) //1MHz
	{
		if(hardMilliseconds)
		{
			UpdateClock();
			ReadInputs();
			UpdateState();

			lostMilliseconds = (hardMilliseconds-1);
			hardMilliseconds = 0;
		}
   	}

}


// Timer A0 interrupt service routine
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
	++hardMilliseconds;
}

//TODO: determine interrupt nesting behavior.
// Port 1 (external power loss) interrupt service routine
#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
	uint32_t i;

	__disable_interrupt();
	P1IFG &= ~BIT0;         // clear interrupt flag

	if (state == UNINIT_STATE)
	{
		//WARNING_LAMP_INIT; // Must initialize to turn off lamp. Serial TX can get stuck on during interrupted TX.
		INSPECTION_LAMP_OFF;
		WARNING_LAMP_OFF;
		COUPLE_OUTPUT_OFF;
		WARNING_OUTPUT_OFF;
	}
	else
	{
		INSPECTION_LAMP_OFF;
		WARNING_LAMP_OFF;
		COUPLE_OUTPUT_OFF;
		WARNING_OUTPUT_OFF;

		UpdateRecordsAndLogState(&recordCurrentSystem, lastState);
	}


	// wait to confirm power loss
	for(i=NOPS_AT_POWER_DOWN ;i>0;i--)
		{
			_nop();
		}

	while ((P1IN & BIT0)==0) //wait for power to return
	{
		_nop();
	}
	//if external power is restored, force reset
	P1IFG &= ~BIT0;         // clear interrupt flag
	__enable_interrupt();
	WDTCTL = ~WDTPW | WDTHOLD;	// write to WDTCTL with wrong password to initiate reset


}


static void UpdateClock(void)
{
	++sysTick_1ms;                        // update elapsed milli-seconds counter
	if (sysTick_1ms >= MS_PER_MIN)
	{
		sysTick_1ms = 0;
		++sysTick_1min;
	}
}

static void ReadInputs(void)
{
	// read kingpin sensor input, left
	if (KPS_IS_ACTIVE == kps) //no pending change
	{
#ifdef COUNT_SENSOR_ERROR
		if (kpsDebounce_ms > 0)
		{
			kpsErr_ms = kpsDebounce_ms;// old Debounce_ms is error data: sensor assertion that did not result in a change of state
		}
#endif
		kpsDebounce_ms = 0;
	}
	else //pending change
	{
		kpsDebounce_ms++;
		if (kpsDebounce_ms >= minValidKpsInputTime) //change state
		{
#ifdef COUNT_SENSOR_ERROR
			kpsDebounce_ms = 0; //clear Debounce to prevent false error record
#endif
			kps = !kps;
			if (kps==TRUE)
			{
				kpTimeAsserted_ms = sysTick_1ms;//tracking only combined kp time //kpsTimeAsserted_ms = sysTick_1ms;
			}
		}
	}

	kp = kps; //(kps || kpsr);

	// read lock sensor input
	if (LKS_IS_ACTIVE == lks) //no pending change (active-low signal)
	{
#ifdef COUNT_SENSOR_ERROR
		if (lksDebounce_ms > 0)
		{
			lksErr_ms = lksDebounce_ms;// old kpsrDebounce_ms is error data: sensor assertion that did not result in a change of state
		}
#endif
		lksDebounce_ms = 0;		
	}
	else //pending change
	{
		lksDebounce_ms++;
		if (lksDebounce_ms  >= MIN_VALID_LOCK_SENSOR_INPUT_TIME_MS) //change state
		{
#ifdef COUNT_SENSOR_ERROR
			lksDebounce_ms = 0; //clear Debounce to prevent false error record
#endif
			lks = !lks;
			if (lks==TRUE)
			{
				lksTimeAsserted_ms = sysTick_1ms;
			}
		}
	}
}

void UpdateState(void)
{
	uint8_t stateChanged = FALSE;
	if (state != lastState)
	{
		if ((lastState == ERROR_STATE) ) // warning signal could be on, so serial transmit won't work.
		{
			WARNING_OUTPUT_OFF; //this may need a delay to provide minimum pulse size.
			//delayTxFlag = TRUE;
		}

		UpdateRecordsAndLogState(&recordCurrentSystem, lastState);

		lastState = state;
		stateChanged = TRUE;
	}

	switch(state)
	{
	case UNINIT_STATE:
		HandleUninitState(stateChanged);
		break;
	case INIT_STATE: //reset timers and activate indicators, check inputs
		HandleInitState(stateChanged);
		break;
	case OPEN_STATE: //wait for inputs, (ready to couple)
		HandleOpenState(stateChanged);
		break;
	case INDETERMINATE_STATE: //not ready to couple, check time of sensor assertion  (use kpTimeAsserted_ms)
		HandleIndeterminateState(stateChanged);
		break;
	case INSPECTION_STATE: //indicate locked
		HandleInspectionState(stateChanged);
		break;
	case LOCK_MONITORING_STATE: //indicate locked
		HandleLockMonitoringState(stateChanged);
		break;
	case UNLOCKED_STATE: //loss of lock, indicate pending error
		HandleUnlockedState(stateChanged);
		break;
	case ERROR_STATE: //indicate error
		HandleErrorState(stateChanged);
		break;
	}
}

static void HandleUninitState(uint8_t stateChanged)
{
	SelectEmptyLog();


	sysTick_1ms = recordCurrentSystem.sysTime_ms;
	sysTick_1min = recordCurrentSystem.sysTime_min;

	state = INIT_STATE;
}

static void HandleInitState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		//UartTx("INIT_STATE\r\n");
		printf("INIT_STATE\r\n");
#endif
		//delay to ensure print output before WARNING_LAMP_INIT
		uint16_t i;
		for(i=10000 ;i>0;i--)
		{
			_nop();
		}

		// Turn on outputs

		INSPECTION_LAMP_ON;
		COUPLE_OUTPUT_ON;
		WARNING_LAMP_INIT;
		WARNING_LAMP_ON;
		WARNING_OUTPUT_ON; //for test only? this could annoyingly activate an external horn

		unlockedTime_ms = 0;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;
		loadSwitchState =  0xFFFF; //ST_WO | ST_CO | ST_WL | ST_IL;

	}

	if (initTimer == (initTimerMaxValue - LOAD_SWITCH_OVER_CURRENT_TEST_TIME_MS)) //LOAD_SWITCH_OVER_CURRENT_TEST_TIME_MS needs to be 0 to detect an over-current signal before it could represent open-circuit (in 700us)
	{
		uint16_t i;
		for(i=LOAD_SWITCH_OVER_CURRENT_TEST_TIME_NOPS ;i>0;i--) //this is to implement a sub-millisecond delay to allow the over-current signal to assert
				{
					_nop();
				}

		if(!LOAD_SWITCH_STATE_WO)
		{
			loadSwitchState &= ~ST_SHORT_WO;
		}
		if(!LOAD_SWITCH_STATE_CO)
		{
			loadSwitchState &= ~ST_SHORT_CO;
		}
		if(!LOAD_SWITCH_STATE_WL)
		{
			loadSwitchState &= ~ST_SHORT_WL;
		}
		if(!LOAD_SWITCH_STATE_IL)
		{
			loadSwitchState &= ~ST_SHORT_IL;
		}
	}

	if (initTimer == (initTimerMaxValue - LOAD_SWITCH_OPEN_CIRCUIT_TEST_TIME_MS))
	{
		if(!LOAD_SWITCH_STATE_WO)
		{
			loadSwitchState &= ~ST_OPEN_WO;
		}
		if(!LOAD_SWITCH_STATE_CO)
		{
			loadSwitchState &= ~ST_OPEN_CO;
		}
		if(!LOAD_SWITCH_STATE_WL)
		{
			loadSwitchState &= ~ST_OPEN_WL;
		}
		if(!LOAD_SWITCH_STATE_IL)
		{
			loadSwitchState &= ~ST_OPEN_IL;
		}
	}

	if (initTimer == (initTimerMaxValue - OUTPUT_INIT_TIME_MS)) //turn off output signals (to prevent nuisance horn activation or prolonged dash gauge signal ambiguity)
	{
		WARNING_OUTPUT_OFF;
		COUPLE_OUTPUT_OFF;
	}

	if (initTimer > 0) //value set at power-on-reset, and below for re-initialize
	{
		initTimer--;
	}
	else //initialization time is passed
	{
		initTimer = REINIT_TIME_MS;
		initTimerMaxValue = REINIT_TIME_MS;
		// Turn off all outputs

		WARNING_LAMP_OFF;
		WARNING_OUTPUT_OFF;
		INSPECTION_LAMP_OFF;
		COUPLE_OUTPUT_OFF;

#ifdef PRINT_SWITCH_STATUS_AFTER_INIT

		WARNING_LAMP_TXENA;

		printf(" inputs test: KP=");
		if (KPS_IS_ACTIVE) printf("on (grounded)");
		else printf("off (pulled-up)");

		printf(" LK=");
		if (LKS_IS_ACTIVE) printf("on (grounded)");
		else printf("off (pulled-up)");

		printf("\r\n");


		printf(" outputs test: WO=");
		if (~loadSwitchState & ST_SHORT_WO) printf("SHORT");
		  else if(~loadSwitchState & ST_OPEN_WO) printf("open");
		  else printf("on");

		printf(" CO=");
		if (~loadSwitchState & ST_SHORT_CO) printf("SHORT");
		  else if(~loadSwitchState & ST_OPEN_CO) printf("open");
		  else printf("on");

		printf(" WL=");
		if (~loadSwitchState & ST_SHORT_WL) printf("SHORT");
		  else if(~loadSwitchState & ST_OPEN_WL) printf("OPEN");
		  else printf("on");

		printf(" IL=");
		if (~loadSwitchState & ST_SHORT_IL) printf("SHORT");
		  else if(~loadSwitchState & ST_OPEN_IL) printf("OPEN");
		  else printf("on");
		printf("\r\n\r\r");

		WARNING_LAMP_INIT;
#endif

		if((kp == TRUE) && (lks == TRUE))
		{
			state = INSPECTION_STATE; //allow jump to INSPECTION_STATE only at power-on
		}

		if ((kp == FALSE) && (lks == FALSE))
		{
			state = OPEN_STATE;
		}
		else
		{
			state = INDETERMINATE_STATE; //TODO: consider changing this to directly error.
		}
	}
}

static void HandleOpenState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("OPEN_STATE\r\n");
#endif
		// Turn off all outputs

		WARNING_LAMP_OFF;
		WARNING_OUTPUT_OFF;
		INSPECTION_LAMP_OFF;
		COUPLE_OUTPUT_OFF;

		unlockedTime_ms = 0;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;

#ifdef TX_SYSTEM_RECORD_IN_OPEN_STATE
		TxSystemRecord(&recordCurrentSystem);
#endif
	}

	if ((kp == TRUE) || (lks == TRUE))
	{
		state = INDETERMINATE_STATE;
	}
}

static void HandleIndeterminateState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("INDETERMINATE_STATE\r\n");
#endif
		// Turn off all outputs

		WARNING_LAMP_OFF;
		WARNING_OUTPUT_OFF;
		INSPECTION_LAMP_OFF;
		COUPLE_OUTPUT_OFF;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;
	}

	if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE)) //normal sequence. wait for lks, else timeout error: lks-delay
	{
		if ((sysTick_1ms - kpTimeAsserted_ms) >= MAX_LKS_DELAY_TIME_MS)
		{
			state = ERROR_STATE;  //TODO ALW NOTE: verify rollover performance, assumes kpTimeAsserted_ms is valid
		}
		else
		{
			state = INDETERMINATE_STATE;
		}
	}
	else if ((kp == FALSE) && (lks == TRUE)) //abnormal sequence. error: no kingpin
	{
		if ((sysTick_1ms - lksTimeAsserted_ms) >= MAX_LKS_EARLY_TIME_MS) //reject abnormal sequence if lock was not recently detected e.g. slow flag assembly
		{
			state = ERROR_STATE;  //TODO ALW NOTE: verify rollover performance, assumes kpTimeAsserted_ms is valid
		}
		else
		{
			state = INDETERMINATE_STATE;
		}

		//state = ERROR_STATE;
	}
	else //(kp == TRUE) && (lks == TRUE)
	{
		state = INSPECTION_STATE; //normal sequence. lks detected after kingpin
	}

}

static void HandleInspectionState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("INSPECTION_STATE\r\n");
#endif
		coupleCount++;
		timeCoupled_min = sysTick_1min;

		// Turn on Inspection light and coupling output
		INSPECTION_LAMP_ON;
		COUPLE_OUTPUT_ON;

		WARNING_LAMP_OFF;
		WARNING_OUTPUT_OFF;

		// slow reaction time to accommodate kingpin movement, but not as slow as while locked.
		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_INSPECT_MS;
	}

	if ((sysTick_1min - timeCoupled_min) >= MAX_INSPECTION_LAMP_TIME_MINUTES)
	{
		state = LOCK_MONITORING_STATE;
	}
	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //loss of lock, pending error: no lock
	}
	else if ((kp == FALSE) && (lks == TRUE))
	{
		state = ERROR_STATE; //loss of kingpin
	}
	else //((kp == TRUE) && (lks == TRUE))
	{
		state = INSPECTION_STATE;
	}
}

static void HandleLockMonitoringState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("LOCK_MONITORING_STATE\r\n");
#endif
		// Turn on Coupling output
		COUPLE_OUTPUT_ON;
		INSPECTION_LAMP_OFF;

		WARNING_LAMP_OFF;
		WARNING_OUTPUT_OFF;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_LOCKED_MS;  //slow reaction time to accommodate kingpin movement.
	}

	if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //loss of lock, pending error: no lock
	}
	else if ((kp == FALSE) && (lks == TRUE))
	{
		state = ERROR_STATE; //sustained loss of kingpin
	}
	else //((kp == TRUE) && (lks == TRUE))
	{
		state = LOCK_MONITORING_STATE;
	}
}

static void HandleUnlockedState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("UNLOCKED_STATE\r\n");
#endif
		INSPECTION_LAMP_OFF;
		COUPLE_OUTPUT_OFF;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;
	}

	// flash lights, click piezo, and 1ms pulses to WARNING_OUTPUT

	WARNING_OUTPUT_OFF;
	ActivateWarningOutputs();

	unlockedTime_ms++;
	if ((unlockedTime_ms >= MAX_TIME_AFTER_UNLOCK_BEFORE_ALARM_MS)
		|| ((kp == FALSE) && (lks == TRUE)) 	//invalid lock, error: no kingpin
		|| ((kp == TRUE) && (lks == TRUE))) 	//unverified lock; user must uncouple and open lock or cycle power to reset
	{
		state = ERROR_STATE;
	}
	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else if ((kp == TRUE) && (lks == FALSE))
	{
		state = UNLOCKED_STATE; //persisting loss of lock, pending error: no lks
	}
}

static void HandleErrorState(uint8_t stateChanged)
{
	if (stateChanged)
	{
#ifdef PRINT_STATE_IN_CASE
		UartTx("ERROR_STATE\r\n");
#endif
		timeError_min = sysTick_1min;
		errorCount++;
		INSPECTION_LAMP_OFF;
		COUPLE_OUTPUT_OFF;

		minValidKpsInputTime = MIN_VALID_KINGPIN_SENSOR_INPUT_TIME_WHILE_OPEN_MS;
	}

	ActivateWarningOutputs(); //and beep, and pulse WARNING_OUTPUT

	if ((sysTick_1min - timeError_min) >= MAX_ERROR_TIME_MINUTES)
	{
		state = INIT_STATE;
	}

	else if ((kp == FALSE) && (lks == FALSE))
	{
		state = OPEN_STATE;
	}
	else
	{
		//((kp == TRUE)&&(lks == FALSE)) persisting loss of lock, error: no lks
		//((kp == FALSE)&&(lks == TRUE)) persisting loss of lock, error: no kingpin
		//((kp == TRUE)&&(lks == TRUE)) unverified lock; user must uncouple and open lock or cycle power to reset
		state = ERROR_STATE;
	}

}

static void ActivateWarningOutputs(void)
{
	blinkTimer--;

	if (blinkTimer <= 0)
	{
		blinkTimer = BLINK_HALF_PERIOD_MS;

		WARNING_LAMP_FLIP;

		if (state != UNLOCKED_STATE)
		{
			 WARNING_OUTPUT_ON; //WARNING_OUTPUT_FLIP;
		}
	}
}




