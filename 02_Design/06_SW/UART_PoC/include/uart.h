/*
 * uart.h
 *
 *  Created on: Feb 4, 2016
 *      Author: robertvanvossen
 */

#ifndef UART_H_
#define UART_H_

void UartInit(void);
void UartTx(char * tx_data);           // serial output transmitter
void putc(char c, void *stream);
int getc(void* stream);
void UartHexAddrTx(uint16_t * addr);

#endif /* UART_H_ */
