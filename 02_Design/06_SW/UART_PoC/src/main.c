#include <stdint.h>
#include <msp430g2755.h>

#include "uart.h"

#define SER_TX_BIT      BIT4


/*
 * main.c
 */
int main(void) {
    char readVal;
    char *idString = "\rUART ECHO PROGRAM\n\r";
    uint8_t index = 0;
    void *nullPtr = 0x00;

    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    // Use calibration values for 1MHz Clock DCO
    DCOCTL  = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL  = CALDCO_1MHZ;

    // Initialize UART peripheral
    UartInit();

    while(idString[index])
    {
        putc(idString[index], nullPtr);
        index++;
    }

    /*************************************************************
     * SPI peripheral test
     *************************************************************/
    while(1)
    {
        // Use the UART to echo whatever is received
        readVal = getc(nullPtr);

        if(readVal)
        {
            putc(readVal, nullPtr);
        }
    }
}
