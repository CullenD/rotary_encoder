/****************************************************************************************
 *  MSP430
 *
 *  Description: SAF-HOLLAND Coupling Monitor
 *               UART and Serial handling.
 *
 * Andrew Wallner / Brian Collins
 *
 ***************************************************************************************/
#include <msp430g2755.h>
#include <stdint.h>
#include <stdlib.h>
#include "uart.h"

#define BAUDRATE_9600 104
#define UART_INIT_DELAY 10000

#define SER_RX_BIT      BIT5
#define SER_TX_BIT      BIT4

void uartEnable(void)
{
    // Direction as input
    P3DIR = 0x00;

    // Select TX functionality for P3.4, RX for P3.5
    P3SEL |= (SER_TX_BIT | SER_RX_BIT);
}

void UartInit(void)
{
	uint16_t i;

	// Put USCI module in reset
	UCA0CTL1 |= UCSWRST;

	UCA0CTL1 |= UCSSEL_2;           // Have USCI use System Master Clock (1MHz)
	UCA0BR0 = BAUDRATE_9600;        // 1MHz, 9600
	UCA0BR1 = 0;

	UCA0MCTL = UCBRS0;              // Modulation UCBRSx = 1

	// Configure peripheral multiplexer to route USCI signals to the external pins
	uartEnable();

	UCA0CTL1 &= ~UCSWRST;           // Start USCI state machine

	// delay to allow (external) serial adaptor to power-up
	for(i=UART_INIT_DELAY;i>0;i--)
	{
		_nop();        		// delay to allow (external) serial adaptor to power-up
	}
}

void UartTx(char * tx_data)
{
	uint16_t i=0;
	while(tx_data[i]) // Incrementing through array, look for null pointer (0) at end of string
	{
		while ((UCA0STAT & UCBUSY)); // Wait if line TX/RX module is busy with data
		UCA0TXBUF = tx_data[i]; // Send out element i of tx_data array on UART bus
		i++; // Increment variable for array address
	}
}


void putc(char c, void *stream)
{
    // Wait if line TX/RX module is busy with data
    while ((UCA0STAT & UCBUSY));

    // Send out c on UART bus
    UCA0TXBUF = c;
}

int getc(void* stream)
{
    // Wait until data is received
    while (!(UC0IFG & UCA0RXIFG));

    return (int)UCA0RXBUF;
}

